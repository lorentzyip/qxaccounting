﻿using QXAccounting.DAL.Data;
using QXAccounting.DAL.Repositories;
using QXAccounting.Models.Entities;
using QXAccounting.Tests.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace QXAccounting.Tests.DAL
{
    public class CompanyRepositoryTest : BaseTestCase
    {
        private readonly CompanyRepository _companyRepo;
        private readonly ShareholderRepository _shareholderRepo;
        private readonly ITestOutputHelper _output;

        public CompanyRepositoryTest(ITestOutputHelper output)
        {
            _companyRepo = new CompanyRepository(new ShareholderRepository());
            _shareholderRepo = new ShareholderRepository();
            _output = output;
            CleanDb();
        }

        private void CleanDb()
        {
            _output.WriteLine("Cleaning DB");
            DbInitializer.ClearData(_companyRepo.Context);
            DbInitializer.SeedData(_companyRepo.Context);
        }

        public override void Dispose()
        {
            //CleanDB();
            _companyRepo.Dispose();
        }

        [Fact]
        public void AddCompany()
        {
            var count = _companyRepo.Count();

            var companyToAdd = new Company
            {
                CompanyName = "公司一",
                Capital = 2500,
                Remarks = "測試公司一"
            };

            _companyRepo.Add(companyToAdd, "testing");

            Assert.Equal(_companyRepo.Count(), count + 1);
        }

        [Fact]
        public void RemoveCompany()
        {
            var company = new Company
            {
                CompanyName = "公司二",
                Capital = 2500,
                Remarks = "測試公司二"
            };

            _companyRepo.Add(company, "testing");

            int count = _companyRepo.Count();

            _companyRepo.Delete(company, "testing");

            // Company count should not be affected
            Assert.Equal(count, _companyRepo.Count());

            Assert.True(company.Deleted);
        }

        [Fact]
        public void AddShareholder()
        {
            var company = new Company
            {
                CompanyName = "公司三",
                Capital = 2500,
                Remarks = "測試公司三"
            };

            _companyRepo.Add(company, "testing");

            var shareholders = _companyRepo.GetShareholders(company.Id).ToList();
            Assert.Equal(0, shareholders.Count);

            var shareholder = new Shareholder
            {
                CompanyId = company.Id,
                ShareholderName = "AAAA",
                Share = 20
            };

            _shareholderRepo.Add(shareholder, "testing");

            //company.Shareholders.Add(shareholder);

            //_companyRepo.Update(company, "testing");

            shareholders = _companyRepo.GetShareholders(company.Id).ToList();
            Assert.Equal(1, shareholders.Count);
        }

        [Fact]
        public void AddShareholder2()
        {
            var company = new Company
            {
                CompanyName = "公司三",
                Capital = 5500,
                Remarks = "測試公司三",
            };

            _companyRepo.Add(company, "testing");

            var shareholders = _companyRepo.GetShareholders(company.Id).ToList();

            Assert.Equal(0, shareholders.Count);

            var shareholder = new Shareholder
            {
                ShareholderName = "AAAA",
                Share = 20
            };

            _companyRepo.AddShareholder(company, shareholder, "testing");

            shareholders = _companyRepo.GetShareholders(company.Id).ToList();
            Assert.Equal(1, shareholders.Count);
        }

        [Fact]
        public void RemoveShareholder()
        {
            var company = new Company
            {
                CompanyName = "公司四",
                Capital = 5500,
                Remarks = "測試公司四",

                Shareholders = new List<Shareholder>
                {
                    new Shareholder
                    {
                        ShareholderName = "Shareholder 1",
                        Share = 50
                    },
                    new Shareholder
                    {
                        ShareholderName = "Shareholder 2",
                        Share = 60
                    }
                }
            };

            _companyRepo.Add(company, "testing");

            Assert.Equal(2, _companyRepo.GetShareholders(company.Id).ToList().Count());

            _shareholderRepo.Delete(company.Shareholders.SingleOrDefault(s => s.ShareholderName == "Shareholder 2"), "testing");

            Assert.True(company.Shareholders.SingleOrDefault(s => s.ShareholderName == "Shareholder 2").Deleted);

            Assert.Equal(2, _companyRepo.GetShareholders(company.Id).ToList().Count());
        }
    }
}
