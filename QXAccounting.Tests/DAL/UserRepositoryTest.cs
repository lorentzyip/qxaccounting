﻿using QXAccounting.DAL.Data;
using QXAccounting.DAL.Repositories;
using QXAccounting.WebApi.ViewModels;
using QXAccounting.Tests.Base;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using AutoMapper;
using QXAccounting.WebApi;
using QXAccounting.Models.Entities;
using Xunit.Abstractions;

namespace QXAccounting.Tests.DAL
{
    [TestCaseOrderer(TestCollectionOrderer.TypeName, TestCollectionOrderer.AssemblyName)]
    public class UserRepositoryTest : BaseTestCase
    {
        private readonly UserRepository _repo;
        private readonly IMapper _mapper;
        private readonly ITestOutputHelper _output;

        public UserRepositoryTest(ITestOutputHelper output)
        {
            _repo = new UserRepository(new UserRoleRepository(), new UserFeatureRepository());
            _mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>()));
            _output = output;
            CleanDb();
        }

        private void CleanDb() {
            _output.WriteLine("Cleaning Database");
            DbInitializer.ClearData(_repo.Context);
            DbInitializer.SeedData(_repo.Context);
        }

        public override void Dispose()
        {
            _repo.Dispose();
        }

        [Fact, TestPriority(1)]
        public void AddUser()
        {
            var userCount = _repo.Count();

            var newUser = new NewUserVM
            {
                LoginId = "test5",
                UserName = "Test user #5",
                Password = "123456789",
                TelephoneNumbers = "+85362278260",
                Email = "test5@test.com"
            };
            var userId = _repo.AddUser(_mapper.Map<NewUserVM, User>(newUser), newUser.Password);
            Assert.Equal(userId, userCount + 1);
        }

        [Fact, TestPriority(2)]
        public void AddExistingUser()
        {
            var newUser = new NewUserVM
            {
                LoginId = "admin",
                UserName = "Administrator",
                Password = "1234"
            };
            var userId = _repo.AddUser(_mapper.Map<NewUserVM, User>(newUser), newUser.Password);
            Assert.Equal(userId, 0);
        }
    }
}
