﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace QXAccounting.WebApi.ViewModels
{
    public class ShareholderVM
    {
        [Required]
        [MaxLength(50)]
        public string ShareholderName { get; set; }

        [Column(TypeName = "decimal(5,2)")]
        public decimal Share { get; set; }

        [MaxLength(300)]
        public string Remarks { get; set; }
    }
}
