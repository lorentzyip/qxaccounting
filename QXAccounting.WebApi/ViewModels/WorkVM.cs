﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace QXAccounting.WebApi.ViewModels
{
    public class WorkVM
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public short Session { get; set; }
        [Required]
        [Column(TypeName = "char(1)")]
        public string Status { get; set; }
        [Required]
        [MaxLength(50)]
        public string CustomerName { get; set; }
        [MaxLength(20)]
        public string AccountNumber { get; set; }
        [MaxLength(50)]
        public string WorkerName { get; set; }
        [MaxLength(100)]
        public string Location { get; set; }
        [MaxLength(20)]
        public string TableNumber { get; set; }
        [Column(TypeName = "decimal(10, 4)")]
        public decimal Capital { get; set; }
        [Column(TypeName = "decimal(10, 4)")]
        public decimal? CapitalExtra { get; set; }
        [Column(TypeName = "decimal(4, 2)")]
        public decimal? Odds { get; set; }
        [Column(TypeName = "decimal(10, 4)")]
        public decimal? CustomerWinLoss { get; set; }
        [Column(TypeName = "decimal(10, 4)")]
        public decimal? Rolling { get; set; }
        public short? CommissionRate { get; set; }
        public short? ActualCommissionRate { get; set; }
        [Column(TypeName = "decimal(5, 2)")]
        public decimal? Share { get; set; }
        public int? CurrencyId { get; set; }
        [Column(TypeName = "decimal(6, 3)")]
        public decimal? ExchangeRate { get; set; }
        [MaxLength(500)]
        public string Remarks { get; set; }
        [Column(TypeName = "char(1)")]
        public string CloseFlag { get; set; }
        [MaxLength(20)]
        public string CloseBy { get; set; }
        public DateTime? CloseTime { get; set; }
        [Column(TypeName = "decimal(10, 4)")]
        public decimal? ActualPaid { get; set; }
    }
}
