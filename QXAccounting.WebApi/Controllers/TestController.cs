﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QXAccounting.WebApi.ViewModels;

namespace QXAccounting.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Test")]
    public class TestController : Controller
    {
        [HttpPost]
        public IActionResult Post([FromBody] TestVM data)
        {
            if (data == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}
