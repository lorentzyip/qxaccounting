﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace QXAccounting.WebApi.Helpers
{
    public static class Utils
    {
        public static string GetClaim(IEnumerable<Claim> claims, string type)
        {
            var claim = claims.SingleOrDefault(c => c.Type == type);

            if (claim == null) return null;

            return claim.Value;
        }
    }
}
