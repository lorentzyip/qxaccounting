﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using QXAccounting.Models.Entities;

namespace QXAccounting.DAL.EF
{
    public class QXAccountingContext : DbContext
    {
        // System
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Feature> Features { get; set; }
        public DbSet<RoleFeature> RoleFeatures { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<UserFeature> UserFeatures { get; set; }

        // Accounting
        public DbSet<Company> Companies { get; set; }
        public DbSet<Shareholder> Shareholders { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<ExchangeHistory> ExchangeHistories { get; set; }
        public DbSet<ClosingHistory> ClosingHistories { get; set; }
        public DbSet<Work> Works { get; set; }

        public QXAccountingContext() { }

        public QXAccountingContext(DbContextOptions<QXAccountingContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(
                    "Server=(localdb)\\mssqllocaldb;Database=QXAccounting;Trusted_Connection=True;MultipleActiveResultSets=true"
                );
            }
        }

        //public override int SaveChanges(bool acceptAllChangesOnSuccess)
        //{
        //    OnBeforeSaving();
        //    return base.SaveChanges(acceptAllChangesOnSuccess);
        //}

        //public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        //{
        //    OnBeforeSaving();
        //    return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        //}

        //private void OnBeforeSaving()
        //{
        //    foreach (var entry in ChangeTracker.Entries<EntityBase>())
        //    {
        //        switch (entry.State)
        //        {
        //            case EntityState.Deleted:
        //                entry.State = EntityState.Modified;
        //                entry.CurrentValues["Deleted"] = true;
        //                entry.CurrentValues["DeleteTime"] = DateTime.Now;
        //                break;
        //        }
        //    }
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(e => e.LoginId)
                    .HasName("AK_USERS_LoginId")
                    .IsUnique();

                entity.Property(e => e.InsertBy).HasDefaultValue("system");
                entity.Property(e => e.InsertTime).HasDefaultValueSql("getdate()");
                entity.Property(e => e.Deleted).HasDefaultValue(false);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.InsertBy).HasDefaultValue("system");
                entity.Property(e => e.InsertTime).HasDefaultValueSql("getdate()");
                entity.Property(e => e.Deleted).HasDefaultValue(false);
            });

            modelBuilder.Entity<Feature>(entity =>
            {
                entity.HasIndex(e => e.FeatureCode)
                    .HasName("AK_Features_FeatureCode")
                    .IsUnique();

                entity.Property(e => e.InsertBy).HasDefaultValue("system");
                entity.Property(e => e.InsertTime).HasDefaultValueSql("getdate()");
                entity.Property(e => e.Deleted).HasDefaultValue(false);
            });

            modelBuilder.Entity<RoleFeature>(entity =>
            {
                entity.HasIndex(e => new { e.RoleId, e.FeatureId })
                    .HasName("UQ_RoleFeatures_RoleId_FeatureId")
                    .IsUnique();

                entity.Property(e => e.InsertBy).HasDefaultValue("system");
                entity.Property(e => e.InsertTime).HasDefaultValueSql("getdate()");
                entity.Property(e => e.Deleted).HasDefaultValue(false);
            });

            modelBuilder.Entity<UserFeature>(entity =>
            {
                entity.HasIndex(e => new { e.UserId, e.FeatureId })
                    .HasName("UQ_UserFeatures_UserId_FeatureId")
                    .IsUnique();

                entity.Property(e => e.InsertBy).HasDefaultValue("system");
                entity.Property(e => e.InsertTime).HasDefaultValueSql("getdate()");
                entity.Property(e => e.Deleted).HasDefaultValue(false);
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasIndex(e => new { e.RoleId, e.UserId })
                    .HasName("UQ_UserRoles_RoleId_UserId")
                    .IsUnique();

                entity.Property(e => e.InsertBy).HasDefaultValue("system");
                entity.Property(e => e.InsertTime).HasDefaultValueSql("getdate()");
                entity.Property(e => e.Deleted).HasDefaultValue(false);
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.HasIndex(e => e.CompanyName)
                    .HasName("UQ_Companies_CompanyName")
                    .IsUnique();
                entity.Property(e => e.Capital).HasDefaultValue(0);

                entity.Property(e => e.InsertBy).HasDefaultValue("system");
                entity.Property(e => e.InsertTime).HasDefaultValueSql("getdate()");
                entity.Property(e => e.Deleted).HasDefaultValue(false);
            });

            modelBuilder.Entity<Shareholder>(entity =>
            {
                entity.Property(e => e.Share).HasDefaultValue(0);

                entity.Property(e => e.InsertBy).HasDefaultValue("system");
                entity.Property(e => e.InsertTime).HasDefaultValueSql("getdate()");
                entity.Property(e => e.Deleted).HasDefaultValue(false);
            });

            modelBuilder.Entity<Currency>(entity =>
            {
                entity.Property(e => e.InsertBy).HasDefaultValue("system");
                entity.Property(e => e.InsertTime).HasDefaultValueSql("getdate()");
                entity.Property(e => e.Deleted).HasDefaultValue(false);
            });

            modelBuilder.Entity<ExchangeHistory>(entity =>
            {
                entity.Property(e => e.InsertBy).HasDefaultValue("system");
                entity.Property(e => e.InsertTime).HasDefaultValueSql("getdate()");
                entity.Property(e => e.Deleted).HasDefaultValue(false);
            });

            modelBuilder.Entity<ClosingHistory>(entity =>
            {
                entity.Property(e => e.InsertBy).HasDefaultValue("system");
                entity.Property(e => e.InsertTime).HasDefaultValueSql("getdate()");
                entity.Property(e => e.Deleted).HasDefaultValue(false);
            });

            modelBuilder.Entity<Work>(entity =>
            {
                entity.Property(e => e.CapitalExtra).HasDefaultValue(0m);
                entity.Property(e => e.CloseFlag).HasDefaultValue("N");
                entity.Property(e => e.Status).HasDefaultValue(WorkStatus.New);
                entity.Property(e => e.Share).HasDefaultValue(100m);
                entity.Property(e => e.Type).HasDefaultValue(WorkType.OffTable);

                entity.Property(e => e.InsertBy).HasDefaultValue("system");
                entity.Property(e => e.InsertTime).HasDefaultValueSql("getdate()");
                entity.Property(e => e.Deleted).HasDefaultValue(false);
            });
        }
    }
}
