﻿using Microsoft.EntityFrameworkCore;
using QXAccounting.DAL.EF;
using QXAccounting.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace QXAccounting.DAL.Data
{
    public static class DbInitializer
    {
        public static void Initialize(QXAccountingContext context)
        {
            context.Database.Migrate();
            ClearData(context);
            SeedData(context);
        }

        public static void ClearData(QXAccountingContext context)
        {
            ExecuteDeleteSQL(context, "System.Users");
            ExecuteDeleteSQL(context, "System.Roles");
            ExecuteDeleteSQL(context, "System.Features");
            ExecuteDeleteSQL(context, "System.UserRoles");
            ExecuteDeleteSQL(context, "System.UserFeatures");
            ExecuteDeleteSQL(context, "System.RoleFeatures");
            ExecuteDeleteSQL(context, "Accounting.Companies");
            ExecuteDeleteSQL(context, "Accounting.Currencies");
            ExecuteDeleteSQL(context, "Accounting.Shareholders");
        }

        public static void ExecuteDeleteSQL(QXAccountingContext context, string tableName)
        {
            var sql = $"DELETE FROM {tableName}";
            context.Database.ExecuteSqlCommand(sql);
            sql = $"IF EXISTS (SELECT * FROM sys.identity_columns WHERE CONCAT(OBJECT_SCHEMA_NAME(object_id), '.', OBJECT_NAME(object_id)) = '{tableName}' AND last_value IS NOT NULL) DBCC CHECKIDENT ('{tableName}', RESEED, 0);";
            context.Database.ExecuteSqlCommand(sql);
        }

        public static void SeedData(QXAccountingContext context)
        {
            using (var connection = new SqlConnection(context.Database.GetDbConnection().ConnectionString))
            {
                connection.Open();

                using (var command = new SqlCommand("[System].[uspAddUser]", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@LoginId", "admin");
                    command.Parameters.AddWithValue("@Password", "abcd1234");
                    command.Parameters.AddWithValue("@UserName", "System Administrator");

                    command.ExecuteNonQuery();
                }

                using (var command = new SqlCommand("[System].[uspAddUser]", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@LoginId", "lorentz");
                    command.Parameters.AddWithValue("@Password", "lorentz");
                    command.Parameters.AddWithValue("@UserName", "Lorentz Yip");

                    command.ExecuteNonQuery();
                }

                using (var command = new SqlCommand("[System].[uspAddUser]", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@LoginId", "test1");
                    command.Parameters.AddWithValue("@Password", "test1");
                    command.Parameters.AddWithValue("@UserName", "Testing Account #1");

                    command.ExecuteNonQuery();
                }

                using (var command = new SqlCommand("[System].[uspAddUser]", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@LoginId", "test2");
                    command.Parameters.AddWithValue("@Password", "test2");
                    command.Parameters.AddWithValue("@UserName", "Testing Account #2");

                    command.ExecuteNonQuery();
                }

                using (var command = new SqlCommand("[System].[uspAddUser]", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@LoginId", "test3");
                    command.Parameters.AddWithValue("@Password", "test3");
                    command.Parameters.AddWithValue("@UserName", "Testing Account #3");

                    command.ExecuteNonQuery();
                }
            }

            var users = context.Users.ToList();

            var roles = new List<Role>
            {
                new Role { RoleName = "Administrator" },
                new Role { RoleName = "Accountant" },
                new Role { RoleName = "Testing" }
            };

            context.Roles.AddRange(roles);
            context.SaveChanges();

            var userroles = new List<UserRole>
            {
                new UserRole
                {
                    UserId = users.Single(u => u.LoginId == "admin").Id,
                    RoleId = roles.Single(r => r.RoleName == "Administrator").Id,
                },
                new UserRole
                {
                    UserId = users.Single(u => u.LoginId == "lorentz").Id,
                    RoleId = roles.Single(r => r.RoleName == "Accountant").Id,
                },
                new UserRole
                {
                    UserId = users.Single(u => u.LoginId == "test1").Id,
                    RoleId = roles.Single(r => r.RoleName == "Testing").Id,
                },
                new UserRole
                {
                    UserId = users.Single(u => u.LoginId == "test2").Id,
                    RoleId = roles.Single(r => r.RoleName == "Testing").Id,
                },
                new UserRole
                {
                    UserId = users.Single(u => u.LoginId == "test3").Id,
                    RoleId = roles.Single(r => r.RoleName == "Accountant").Id,
                },
                new UserRole
                {
                    UserId = users.Single(u => u.LoginId == "test3").Id,
                    RoleId = roles.Single(r => r.RoleName == "Testing").Id,
                }
            };

            context.UserRoles.AddRange(userroles);
            context.SaveChanges();

            var features = new List<Feature>
            {
                new Feature { FeatureName = "All Features", FeatureCode = "FALL" },
                new Feature { FeatureName = "Feature 1", FeatureCode = "F001" },
                new Feature { FeatureName = "Feature 2", FeatureCode = "F002" },
                new Feature { FeatureName = "Feature 3", FeatureCode = "F003" },
            };

            context.Features.AddRange(features);
            context.SaveChanges();

            var rolefeatures = new List<RoleFeature>
            {
                new RoleFeature
                {
                    RoleId = roles.Single(r => r.RoleName == "Administrator").Id,
                    FeatureId = features.Single(f => f.FeatureCode == "FALL").Id
                },
                new RoleFeature
                {
                    RoleId = roles.Single(r => r.RoleName == "Accountant").Id,
                    FeatureId = features.Single(f => f.FeatureCode == "F001").Id
                },
                new RoleFeature
                {
                    RoleId = roles.Single(r => r.RoleName == "Accountant").Id,
                    FeatureId = features.Single(f => f.FeatureCode == "F003").Id
                },
                new RoleFeature
                {
                    RoleId = roles.Single(r => r.RoleName == "Testing").Id,
                    FeatureId = features.Single(f => f.FeatureCode == "F001").Id
                }
            };

            context.RoleFeatures.AddRange(rolefeatures);
            context.SaveChanges();

            var userfeatures = new List<UserFeature>
            {
                new UserFeature
                {
                    UserId = users.Single(u => u.LoginId == "test2").Id,
                    FeatureId = features.Single(f => f.FeatureCode == "F002").Id
                },
                new UserFeature
                {
                    UserId = users.Single(u => u.LoginId == "test3").Id,
                    FeatureId = features.Single(f => f.FeatureCode == "F003").Id
                }
            };

            context.UserFeatures.AddRange(userfeatures);
            context.SaveChanges();

            var companies = new List<Company>
            {
                new Company
                {
                    CompanyName = "黃馬公司",
                    Capital = 20000,
                    Shareholders = new List<Shareholder>
                    {
                        new Shareholder
                        {
                            ShareholderName = "楊",
                            Share = 50m
                        },
                        new Shareholder
                        {
                            ShareholderName = "齊",
                            Share = 50m
                        }
                    },
                    Remarks = "菲律宾做B数：人民币最高占成88%佣率20.5+0.5=21（0.5是消费）\n港币最高占成86%佣率20\n塞班B数最高占成：70%"
                },
                new Company
                {
                    CompanyName = "Company B",
                    Capital = 2500,
                    Shareholders = new List<Shareholder>
                    {
                        new Shareholder
                        {
                            ShareholderName = "Shareholder 1",
                            Share = 30
                        },
                        new Shareholder
                        {
                            ShareholderName = "Shareholder 2",
                            Share = 30
                        },
                        new Shareholder
                        {
                            ShareholderName = "Shareholder 3",
                            Share = 40
                        }
                    }
                }
            };

            context.Companies.AddRange(companies);
            context.SaveChanges();

            var currencies = new List<Currency>
            {
                new Currency
                {
                    CurrencyName = "美元",
                    ISO4217 = "USD"
                },
                new Currency
                {
                    CurrencyName = "港幣",
                    ISO4217 = "HKD"
                },
                new Currency
                {
                    CurrencyName = "人民幣",
                    ISO4217 = "CNY"
                }
            };

            context.Currencies.AddRange(currencies);
            context.SaveChanges();
        }
    }
}
