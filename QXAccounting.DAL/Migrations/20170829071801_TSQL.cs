﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace QXAccounting.DAL.Migrations
{
    public partial class TSQL : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string sql =
                "CREATE PROCEDURE [System].[uspAddUser]" + Environment.NewLine +
                "    @LoginId nvarchar(20)," + Environment.NewLine +
                "    @UserName nvarchar(50)," + Environment.NewLine +
                "    @TelephoneNumbers nvarchar(100)=NULL," + Environment.NewLine +
                "    @Email nvarchar(50)=NULL," + Environment.NewLine +
                "    @Password nvarchar(50)," + Environment.NewLine +
                "    @InsertBy nvarchar(20)='system'," + Environment.NewLine +
                "    @Id int=NULL OUTPUT," + Environment.NewLine +
                "    @Response nvarchar(250)='' OUTPUT" + Environment.NewLine +
                "AS" + Environment.NewLine +
                "BEGIN" + Environment.NewLine +
                "    SET NOCOUNT ON" + Environment.NewLine +
                "    DECLARE @Salt uniqueidentifier = NEWID()" + Environment.NewLine +
                "    BEGIN TRY" + Environment.NewLine +
                "        INSERT INTO[System].[Users](LoginId, PasswordHash, Salt, UserName, TelephoneNumbers, Email, InsertBy)" + Environment.NewLine +
                "        VALUES(@LoginId, HASHBYTES('SHA2_512', @Password+CAST(@Salt AS nvarchar(36))), @Salt, @UserName, @TelephoneNumbers, @Email, @InsertBy)" + Environment.NewLine +
                "        SET @Id = SCOPE_IDENTITY();" + Environment.NewLine +
                "        SET @Response = 'Success'" + Environment.NewLine +
                "        RETURN(0)" + Environment.NewLine +
                "    END TRY" + Environment.NewLine +
                "    BEGIN CATCH" + Environment.NewLine +
                "        SET @Response = ERROR_MESSAGE()" + Environment.NewLine +
                "        RETURN(1)" + Environment.NewLine +
                "    END CATCH" + Environment.NewLine +
                "END";

            migrationBuilder.Sql(sql);

            sql =
                "CREATE PROCEDURE [System].[uspUserLogin]" + Environment.NewLine +
                "    @LoginId nvarchar(20)," + Environment.NewLine +
                "    @Password nvarchar(50)," + Environment.NewLine +
                "    @Id int=NULL OUTPUT," + Environment.NewLine +
                "    @Response nvarchar(250)='' OUTPUT" + Environment.NewLine +
                "AS" + Environment.NewLine +
                "BEGIN" + Environment.NewLine +
                "    SET NOCOUNT ON" + Environment.NewLine +
                "    IF EXISTS (SELECT TOP 1 Id FROM [System].[Users] WHERE LoginId=@LoginId)" + Environment.NewLine +
                "    BEGIN" + Environment.NewLine +
                "        SET @Id=(SELECT Id From [System].[Users] WHERE LoginId=@LoginId AND PasswordHash=HASHBYTES('SHA2_512', @Password+CAST(Salt AS nvarchar(36))))" + Environment.NewLine +
                "        IF (@Id IS NULL)" + Environment.NewLine +
                "        BEGIN" + Environment.NewLine +
                "            SET @Response='Incorrect password'" + Environment.NewLine +
                "            RETURN(1)" + Environment.NewLine +
                "        END" + Environment.NewLine +
                "        ELSE" + Environment.NewLine +
                "        BEGIN" + Environment.NewLine +
                "            SET @Response='Logged in'" + Environment.NewLine +
                "            RETURN(0)" + Environment.NewLine +
                "        END" + Environment.NewLine +
                "    END" + Environment.NewLine +
                "    ELSE" + Environment.NewLine +
                "    BEGIN" + Environment.NewLine +
                "        SET @Response='Invalid login'" + Environment.NewLine +
                "        RETURN(2)" + Environment.NewLine +
                "    END" + Environment.NewLine +
                "END";

            migrationBuilder.Sql(sql);

            sql =
                "CREATE PROCEDURE [System].[uspChangeUserPassword]" + Environment.NewLine +
                "    @LoginId nvarchar(20)," + Environment.NewLine +
                "    @OldPassword nvarchar(50)," + Environment.NewLine +
                "    @NewPassword nvarchar(50)," + Environment.NewLine +
                "    @UpdateBy nvarchar(20)='system'," + Environment.NewLine +
                "    @Response nvarchar(250)='' OUTPUT" + Environment.NewLine +
                "AS" + Environment.NewLine +
                "BEGIN" + Environment.NewLine +
                "    SET NOCOUNT ON" + Environment.NewLine +
                "    DECLARE @Id int" + Environment.NewLine +
                "    IF EXISTS (SELECT TOP 1 Id FROM [System].[Users] WHERE LoginId=@LoginId)" + Environment.NewLine +
                "    BEGIN" + Environment.NewLine +
                "        SET @Id=(SELECT Id From [System].[Users] WHERE LoginId=@LoginId AND PasswordHash=HASHBYTES('SHA2_512', @OldPassword+CAST(Salt AS nvarchar(36))))" + Environment.NewLine +
                "        IF (@Id IS NULL)" + Environment.NewLine +
                "        BEGIN" + Environment.NewLine +
                "            SET @Response = 'Incorrect password'" + Environment.NewLine +
                "            RETURN(1)" + Environment.NewLine +
                "        END" + Environment.NewLine +
                "        ELSE" + Environment.NewLine +
                "        BEGIN" + Environment.NewLine +
                "            DECLARE @Salt uniqueidentifier = NEWID()" + Environment.NewLine +
                "            BEGIN TRY" + Environment.NewLine +
                "                UPDATE [System].[Users]" + Environment.NewLine +
                "                SET PasswordHash=HASHBYTES('SHA2_512', @NewPassword+CAST(@Salt AS nvarchar(36)))," + Environment.NewLine +
                "                    Salt=@Salt," + Environment.NewLine +
                "                    UpdateBy=@UpdateBy," + Environment.NewLine +
                "                    UpdateTime=GETDATE()" + Environment.NewLine +
                "                WHERE LoginId=@LoginId" + Environment.NewLine +
                "                SET @Response = 'Success'" + Environment.NewLine +
                "                RETURN(0)" + Environment.NewLine +
                "            END TRY" + Environment.NewLine +
                "            BEGIN CATCH" + Environment.NewLine +
                "                SET @Response = ERROR_MESSAGE()" + Environment.NewLine +
                "                RETURN(2)" + Environment.NewLine +
                "            END CATCH" + Environment.NewLine +
                "        END" + Environment.NewLine +
                "    END" + Environment.NewLine +
                "    ELSE" + Environment.NewLine +
                "    BEGIN" + Environment.NewLine +
                "        SET @Response='No such user'" + Environment.NewLine +
                "        RETURN(3)" + Environment.NewLine +
                "    END" + Environment.NewLine +
                "END";

            migrationBuilder.Sql(sql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
