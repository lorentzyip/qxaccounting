﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace QXAccounting.DAL.Migrations
{
    public partial class ChangeShareholderRemarkToRemarks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Remark",
                schema: "Accounting",
                table: "Shareholders");

            migrationBuilder.AddColumn<string>(
                name: "Remarks",
                schema: "Accounting",
                table: "Shareholders",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Remarks",
                schema: "Accounting",
                table: "Shareholders");

            migrationBuilder.AddColumn<string>(
                name: "Remark",
                schema: "Accounting",
                table: "Shareholders",
                maxLength: 300,
                nullable: true);
        }
    }
}
