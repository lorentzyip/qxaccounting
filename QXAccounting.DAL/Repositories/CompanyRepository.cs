﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using QXAccounting.DAL.EF;
using QXAccounting.DAL.Repositories.Base;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.Models.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace QXAccounting.DAL.Repositories
{
    public class CompanyRepository : RepositoryBase<Company>, ICompanyRepository
    {
        private readonly IShareholderRepository _shareholderRepo;

        public CompanyRepository(
            DbContextOptions<QXAccountingContext> options,
            IShareholderRepository shareholderRepo
        ) : base(options)
        {
            _shareholderRepo = shareholderRepo;
        }

        public CompanyRepository(
            IShareholderRepository shareholderRepo
        ) : base()
        {
            _shareholderRepo = shareholderRepo;
        }

        public IEnumerable<Company> GetAllCompanies()
        {
            return Table.Include(c => c.Shareholders).ToList();
        }

        public async Task<IEnumerable<Company>> GetAllCompaniesAsync()
        {
            return await Table.Include(c => c.Shareholders).ToListAsync();
        }

        public Company GetCompany(int id)
        {
            return Table.Include(c => c.Shareholders).SingleOrDefault(c => c.Id == id);
        }

        public async Task<Company> GetCompanyAsync(int id)
        {
            return await Table.Include(c => c.Shareholders).SingleOrDefaultAsync(c => c.Id == id);
        }

        public override int Delete(Company entity, string account = "system", bool persist = true)
        {
            _shareholderRepo.DeleteRange(entity.Shareholders, account, false);

            return base.Delete(entity, account, persist);
        }

        public override async Task<int> DeleteAsync(Company entity, string account = "system", bool persist = true)
        {
            await _shareholderRepo.DeleteRangeAsync(entity.Shareholders, account, false);

            return await base.DeleteAsync(entity, account, persist);
        }

        public override int DeleteRange(IEnumerable<Company> entities, string account = "system", bool persist = true)
        {
            foreach (var entity in entities)
            {
                _shareholderRepo.DeleteRange(entity.Shareholders, account, false);
            }

            return base.DeleteRange(entities, account, persist);
        }

        public override async Task<int> DeleteRangeAsync(IEnumerable<Company> entities, string account = "system", bool persist = true)
        {
            foreach (var entity in entities)
            {
                await _shareholderRepo.DeleteRangeAsync(entity.Shareholders, account, false);
            }

            return await base.DeleteRangeAsync(entities, account, persist);
        }

        public IEnumerable<Shareholder> GetShareholders(int id)
        {
            var shareholders = _shareholderRepo.GetShareholdersByCompanyId(id);

            return shareholders;
        }

        public async Task<IEnumerable<Shareholder>> GetShareholdersAsync(int id)
        {
            var shareholders = await _shareholderRepo.GetShareholdersByCompanyIdAsync(id);

            return shareholders;
        }

        public int AddShareholder(int companyId, Shareholder shareholder, string account = "system", bool persist = true)
        {
            if (companyId <= 0) return -1;

            if (Table.Find(companyId) == null) return -1;

            shareholder.CompanyId = companyId;

            return _shareholderRepo.Add(shareholder, account, persist);
        }

        public async Task<int> AddShareholderAsync(int companyId, Shareholder shareholder, string account = "system", bool persist = true)
        {
            if (companyId <= 0) return -1;

            if (await Table.FindAsync(companyId) == null) return -1;

            shareholder.CompanyId = companyId;

            return await _shareholderRepo.AddAsync(shareholder, account, persist);
        }

        public int AddShareholder(Company company, Shareholder shareholder, string account = "system", bool persist = true)
        {
            if (company.Id <= 0 || company == null) return -1;

            shareholder.CompanyId = company.Id;

            return _shareholderRepo.Add(shareholder, account, persist);
        }

        public async Task<int> AddShareholderAsync(Company company, Shareholder shareholder, string account = "system", bool persist = true)
        {
            if (company.Id <= 0 || company == null) return -1;

            shareholder.CompanyId = company.Id;

            return await _shareholderRepo.AddAsync(shareholder, account, persist);
        }

        public override int Add(Company company, string account = "system", bool persist = true)
        {
            foreach (var shareholder in company.Shareholders)
            {
                shareholder.InsertBy = account;
            }

            return base.Add(company, account, persist);
        }

        public override async Task<int> AddAsync(Company company, string account = "system", bool persist = true)
        {
            foreach (var shareholder in company.Shareholders)
            {
                shareholder.InsertBy = account;
            }

            return await base.AddAsync(company, account, persist);
        }

        //public int DeleteShareholder(int companyId, Shareholder shareholder, string account = "system", bool persist = true)
        //{
        //    if (companyId <= 0) return -1;

        //    if (Table.Find(companyId) == null) return -1;

        //    shareholder.CompanyId = companyId;

        //    return _shareholderRepo.Add(shareholder, account, persist);
        //}

        //public Task<int> DeleteShareholderAsync(int companyId, Shareholder shareholder, string account = "system", bool persist = true)
        //{
        //    throw new System.NotImplementedException();
        //}

        //public int DeleteShareholder(Company company, Shareholder shareholder, string account = "system", bool persist = true)
        //{
        //    throw new System.NotImplementedException();
        //}

        //public Task<int> DeleteShareholderAsync(Company company, Shareholder shareholder, string account = "system", bool persist = true)
        //{
        //    throw new System.NotImplementedException();
        //}
    }
}
