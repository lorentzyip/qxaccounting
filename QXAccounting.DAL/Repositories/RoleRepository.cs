﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;
using QXAccounting.DAL.Repositories.Base;
using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.DAL.EF;

namespace QXAccounting.DAL.Repositories
{
    public class RoleRepository : RepositoryBase<Role>, IRoleRepository
    {
        private IUserRoleRepository _userRoleRepo;
        private IRoleFeatureRepository _roleFeatureRepo;

        public RoleRepository(
            IUserRoleRepository userRoleRepo,
            IRoleFeatureRepository roleFeatureRepo,
            DbContextOptions<QXAccountingContext> options
        ) : base(options)
        {
            _userRoleRepo = userRoleRepo;
            _roleFeatureRepo = roleFeatureRepo;
        }

        public RoleRepository(
            IUserRoleRepository userRoleRepo,
            IRoleFeatureRepository roleFeatureRepo
        ) : base()
        {
            _userRoleRepo = userRoleRepo;
            _roleFeatureRepo = roleFeatureRepo;
        }

        public override int Delete(Role entity, string account = "system", bool persist = true)
        {
            _roleFeatureRepo.DeleteRange(entity.RoleFeatures, account, false);
            _userRoleRepo.DeleteRange(entity.UserRoles, account, false);

            return base.Delete(entity, account, persist);
        }

        public override async Task<int> DeleteAsync(Role entity, string account = "system", bool persist = true)
        {
            await _roleFeatureRepo.DeleteRangeAsync(entity.RoleFeatures, account, false);
            await _userRoleRepo.DeleteRangeAsync(entity.UserRoles, account, false);

            return await base.DeleteAsync(entity, account, persist);
        }

        public override int DeleteRange(IEnumerable<Role> entities, string account = "system", bool persist = true)
        {
            foreach (var entity in entities)
            {
                _roleFeatureRepo.DeleteRange(entity.RoleFeatures, account, false);
                _userRoleRepo.DeleteRange(entity.UserRoles, account, false);
            }

            return base.DeleteRange(entities, account, persist);
        }

        public override async Task<int> DeleteRangeAsync(IEnumerable<Role> entities, string account = "system", bool persist = true)
        {
            foreach (var entity in entities)
            {
                await _roleFeatureRepo.DeleteRangeAsync(entity.RoleFeatures, account, false);
                await _userRoleRepo.DeleteRangeAsync(entity.UserRoles, account, false);
            }

            return await base.DeleteRangeAsync(entities, account, persist);
        }

        public Role GetRoleFull(int id)
        {
            return Table
                .Include(r => r.RoleFeatures)
                    .ThenInclude(rf => rf.Feature)
                .Include(r => r.UserRoles)
                    .ThenInclude(ur => ur.User)
                .SingleOrDefault(r => r.Id == id);
        }

        public async Task<Role> GetRoleFullAsync(int id)
        {
            return await Table
                .Include(r => r.RoleFeatures)
                    .ThenInclude(rf => rf.Feature)
                .Include(r => r.UserRoles)
                    .ThenInclude(ur => ur.User)
                .SingleOrDefaultAsync(r => r.Id == id);
        }

        public IEnumerable<Role> GetRolesFull()
        {
            return Table
                .Include(r => r.RoleFeatures)
                    .ThenInclude(rf => rf.Feature)
                .Include(r => r.UserRoles)
                    .ThenInclude(ur => ur.User)
                .ToList();
        }

        public async Task<IEnumerable<Role>> GetRolesFullAsync()
        {
            return await Table
                .Include(r => r.RoleFeatures)
                    .ThenInclude(rf => rf.Feature)
                .Include(r => r.UserRoles)
                    .ThenInclude(ur => ur.User)
                .ToListAsync();
        }
    }
}
