﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using QXAccounting.DAL.Repositories.Base;
using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.DAL.EF;

namespace QXAccounting.DAL.Repositories
{
    public class UserFeatureRepository : RepositoryBase<UserFeature>, IUserFeatureRepository
    {
        public UserFeatureRepository(DbContextOptions<QXAccountingContext> options) : base(options)
        { }

        public UserFeatureRepository() : base()
        { }

        public IEnumerable<Feature> GetFeatures(int id)
        {
            var features = Table
                            .Where(uf => uf.Id == id)
                            .Include(uf => uf.Feature)
                            .Select(uf => uf.Feature)
                            .ToList();

            return features;
        }

        public IEnumerable<Feature> GetFeatures(string loginId)
        {
            var user = Context.Users.AsNoTracking().SingleOrDefault(u => u.LoginId == loginId);

            if (user == null)
            {
                return null;
            }

            return GetFeatures(user.Id);
        }

        public async Task<IEnumerable<Feature>> GetFeaturesAsync(int id)
        {
            var features = await Table
                            .Where(uf => uf.Id == id)
                            .Include(uf => uf.Feature)
                            .Select(uf => uf.Feature)
                            .ToListAsync();

            return features;
        }

        public async Task<IEnumerable<Feature>> GetFeaturesAsync(string loginId)
        {
            var user = await Context.Users.AsNoTracking().SingleOrDefaultAsync(u => u.LoginId == loginId);

            if (user == null)
            {
                return null;
            }

            return await GetFeaturesAsync(user.Id);
        }

        public IEnumerable<UserFeature> UpdateUserFeatures(int userId, IEnumerable<int> featureIds, string account = "system", bool persist = true)
        {
            var userFeaturesToUpdate = Table.Where(rf => rf.UserId == userId).OrderBy(rf => rf.FeatureId).ToList();

            if (featureIds == null || featureIds?.Count() == 0)
            {
                base.DeleteRange(userFeaturesToUpdate, account, persist);
            }
            else
            {
                var existingFeatureIds = userFeaturesToUpdate.Select(rf => rf.FeatureId);

                var featureIdsToDelete = existingFeatureIds.Except(featureIds);
                var featureIdsToAdd = featureIds.Except(existingFeatureIds);
                var featureIdsToEnable = existingFeatureIds.Intersect(featureIds);

                var userFeaturesToDelete = userFeaturesToUpdate.Where(rf => featureIdsToDelete.Contains(rf.FeatureId));
                var userFeaturesToEnable = userFeaturesToUpdate.Where(rf => featureIdsToEnable.Contains(rf.FeatureId));

                base.DeleteRange(userFeaturesToDelete, account, false);

                foreach (var userFeature in userFeaturesToEnable)
                {
                    userFeature.Deleted = false;
                    userFeature.DeleteBy = null;
                    userFeature.DeleteTime = null;
                }
                base.UpdateRange(userFeaturesToEnable, account, false);

                foreach (var featureId in featureIdsToAdd)
                {
                    base.Add(new UserFeature { UserId = userId, FeatureId = featureId }, account, false);
                }

                SaveChanges();
            }

            return userFeaturesToUpdate;
        }

        public async Task<IEnumerable<UserFeature>> UpdateUserFeaturesAsync(int userId, IEnumerable<int> featureIds, string account = "system", bool persist = true)
        {
            var userFeaturesToUpdate = Table.Where(rf => rf.UserId == userId).OrderBy(rf => rf.FeatureId).ToList();

            if (featureIds == null || featureIds?.Count() == 0)
            {
                await base.DeleteRangeAsync(userFeaturesToUpdate, account, persist);
            }
            else
            {
                var existingFeatureIds = userFeaturesToUpdate.Select(rf => rf.FeatureId);

                var featureIdsToDelete = existingFeatureIds.Except(featureIds);
                var featureIdsToAdd = featureIds.Except(existingFeatureIds);
                var featureIdsToEnable = existingFeatureIds.Intersect(featureIds);

                var userFeaturesToDelete = userFeaturesToUpdate.Where(rf => featureIdsToDelete.Contains(rf.FeatureId));
                var userFeaturesToEnable = userFeaturesToUpdate.Where(rf => featureIdsToEnable.Contains(rf.FeatureId));

                await base.DeleteRangeAsync(userFeaturesToDelete, account, false);

                foreach (var userFeature in userFeaturesToEnable)
                {
                    userFeature.Deleted = false;
                    userFeature.DeleteBy = null;
                    userFeature.DeleteTime = null;
                }
                await base.UpdateRangeAsync(userFeaturesToEnable, account, false);

                foreach (var featureId in featureIdsToAdd)
                {
                    await base.AddAsync(new UserFeature { UserId = userId, FeatureId = featureId }, account, false);
                }

                await SaveChangesAsync();
            }

            return userFeaturesToUpdate;
        }
    }
}
