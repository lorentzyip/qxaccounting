﻿using Microsoft.EntityFrameworkCore;
using QXAccounting.DAL.EF;
using QXAccounting.DAL.Repositories.Base;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.Models.Entities;

namespace QXAccounting.DAL.Repositories
{
    public class ExchangeHistoryRepository : RepositoryBase<ExchangeHistory>, IExchangeHistoryRepository
    {
        public ExchangeHistoryRepository(DbContextOptions<QXAccountingContext> options) : base(options)
        { }

        public ExchangeHistoryRepository() : base()
        { }
    }
}
