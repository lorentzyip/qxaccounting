﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;
using QXAccounting.DAL.Repositories.Base;
using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.DAL.EF;

namespace QXAccounting.DAL.Repositories
{
    public class FeatureRepository : RepositoryBase<Feature>, IFeatureRepository
    {
        private IRoleFeatureRepository _roleFeatureRepo;
        private IUserFeatureRepository _userFeatureRepo;

        public FeatureRepository(
            IRoleFeatureRepository roleFeatureRepo,
            IUserFeatureRepository userFeatureRepo,
            DbContextOptions<QXAccountingContext> options
        ) : base(options)
        {
            _roleFeatureRepo = roleFeatureRepo;
            _userFeatureRepo = userFeatureRepo;
        }

        public FeatureRepository(
            IRoleFeatureRepository roleFeatureRepo,
            IUserFeatureRepository userFeatureRepo
        ) : base()
        {
            _roleFeatureRepo = roleFeatureRepo;
            _userFeatureRepo = userFeatureRepo;
        }

        public override int Delete(Feature entity, string account = "system", bool persist = true)
        {
            _roleFeatureRepo.DeleteRange(entity.RoleFeatures, account, false);
            _userFeatureRepo.DeleteRange(entity.UserFeatures, account, false);

            return base.Delete(entity, account, persist);
        }

        public override async Task<int> DeleteAsync(Feature entity, string account = "system", bool persist = true)
        {
            await _roleFeatureRepo.DeleteRangeAsync(entity.RoleFeatures, account, false);
            await _userFeatureRepo.DeleteRangeAsync(entity.UserFeatures, account, false);

            return await base.DeleteAsync(entity, account, persist);
        }

        public override int DeleteRange(IEnumerable<Feature> entities, string account = "system", bool persist = true)
        {
            foreach (var entity in entities)
            {
                _roleFeatureRepo.DeleteRange(entity.RoleFeatures, account, false);
                _userFeatureRepo.DeleteRange(entity.UserFeatures, account, false);
            }

            return base.DeleteRange(entities, account, persist);
        }

        public override async Task<int> DeleteRangeAsync(IEnumerable<Feature> entities, string account = "system", bool persist = true)
        {
            foreach (var entity in entities)
            {
                await _roleFeatureRepo.DeleteRangeAsync(entity.RoleFeatures, account, false);
                await _userFeatureRepo.DeleteRangeAsync(entity.UserFeatures, account, false);
            }

            return await base.DeleteRangeAsync(entities, account, persist);
        }

        public Feature GetFeature(string featureCode)
        {
            return Table.SingleOrDefault(f => f.FeatureCode == featureCode);
        }

        public async Task<Feature> GetFeatureAsync(string featureCode)
        {
            return await Table.SingleOrDefaultAsync(f => f.FeatureCode == featureCode);
        }

        public Feature GetFeatureFull(string featureCode)
        {
            return Table
                    .Include(f => f.RoleFeatures)
                        .ThenInclude(rf => rf.Role)
                    .Include(f => f.UserFeatures)
                        .ThenInclude(uf => uf.User)
                    .SingleOrDefault(f => f.FeatureCode == featureCode);
        }

        public Feature GetFeatureFull(int id)
        {
            return Table
                    .Include(f => f.RoleFeatures)
                        .ThenInclude(rf => rf.Role)
                    .Include(f => f.UserFeatures)
                        .ThenInclude(uf => uf.User)
                    .SingleOrDefault(f => f.Id == id);
        }

        public async Task<Feature> GetFeatureFullAsync(string featureCode)
        {
            return await Table
                    .Include(f => f.RoleFeatures)
                        .ThenInclude(rf => rf.Role)
                    .Include(f => f.UserFeatures)
                        .ThenInclude(uf => uf.User)
                    .SingleOrDefaultAsync(f => f.FeatureCode == featureCode);
        }

        public async Task<Feature> GetFeatureFullAsync(int id)
        {
            return await Table
                    .Include(f => f.RoleFeatures)
                        .ThenInclude(rf => rf.Role)
                    .Include(f => f.UserFeatures)
                        .ThenInclude(uf => uf.User)
                    .SingleOrDefaultAsync(f => f.Id == id);
        }

        public IEnumerable<Feature> GetFeaturesFull()
        {
            return Table
                    .Include(f => f.RoleFeatures)
                        .ThenInclude(rf => rf.Role)
                    .Include(f => f.UserFeatures)
                        .ThenInclude(uf => uf.User)
                    .ToList();
        }

        public async Task<IEnumerable<Feature>> GetFeaturesFullAsync()
        {
            return await Table
                    .Include(f => f.RoleFeatures)
                        .ThenInclude(rf => rf.Role)
                    .Include(f => f.UserFeatures)
                        .ThenInclude(uf => uf.User)
                    .ToListAsync();
        }
    }
}
