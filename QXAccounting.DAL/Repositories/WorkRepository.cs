﻿using Microsoft.EntityFrameworkCore;
using QXAccounting.DAL.Repositories.Base;
using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.DAL.EF;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace QXAccounting.DAL.Repositories
{
    public class WorkRepository : RepositoryBase<Work>, IWorkRepository
    {
        private readonly IClosingHistoryRepository _closingHistoryRepo;
        private readonly IExchangeHistoryRepository _exchangeHistoryRepo;

        public WorkRepository(
            DbContextOptions<QXAccountingContext> options,
            IClosingHistoryRepository closingHistoryRepo,
            IExchangeHistoryRepository exchangeHistoryRepo
        ) : base(options)
        {
            _closingHistoryRepo = closingHistoryRepo;
            _exchangeHistoryRepo = exchangeHistoryRepo;
        }

        public WorkRepository() : base()
        { }

        public int close(string account = "system")
        {
            throw new System.NotImplementedException();
        }

        public int UpdateWork(Work work, string account = "system", bool persist = true)
        {
            return Update(work, account, persist);
        }

        public async Task<int> UpdateWorkAsync(Work work, string account = "system", bool persist = true)
        {
            // should check the changes of commission rate and exchange rate, if different, make a record in exhange histories
            throw new System.NotImplementedException();
        }

        public IEnumerable<Work> GetAllWorks()
        {
            return Table
                    .Include(w => w.ClosingHistories)
                    .Include(w => w.ExchangeHistories)
                    .ToList();
        }

        public async Task<IEnumerable<Work>> GetAllWorksAsync()
        {
            return await Table
                            .Include(w => w.ClosingHistories)
                            .Include(w => w.ExchangeHistories)
                            .ToListAsync();
        }

        public Work GetWork(int id)
        {
            return Table
                    .Include(w => w.ClosingHistories)
                    .Include(w => w.ExchangeHistories)
                    .SingleOrDefault(w => w.Id == id);
        }

        public async Task<Work> GetWorkAsync(int id)
        {
            return await Table
                            .Include(w => w.ClosingHistories)
                            .Include(w => w.ExchangeHistories)
                            .SingleOrDefaultAsync(w => w.Id == id);
        }
    }
}