﻿using Microsoft.EntityFrameworkCore;
using QXAccounting.DAL.EF;
using QXAccounting.DAL.Repositories.Base;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.Models.Entities;

namespace QXAccounting.DAL.Repositories
{
    public class CurrencyRepository : RepositoryBase<Currency>, ICurrencyRepository
    {
        public CurrencyRepository(DbContextOptions<QXAccountingContext> options) : base(options)
        { }

        public CurrencyRepository() : base()
        { }
    }
}