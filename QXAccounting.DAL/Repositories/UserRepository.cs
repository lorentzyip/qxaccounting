﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using QXAccounting.DAL.Repositories.Base;
using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.DAL.EF;

namespace QXAccounting.DAL.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        private IUserRoleRepository _userRoleRepo;
        private IUserFeatureRepository _userFeatureRepo;

        public UserRepository(
            DbContextOptions<QXAccountingContext> options,
            IUserRoleRepository userRoleRepo,
            IUserFeatureRepository userFeatureRepo
        ) : base(options)
        {
            _userRoleRepo = userRoleRepo;
            _userFeatureRepo = userFeatureRepo;
        }

        public UserRepository(
            IUserRoleRepository userRoleRepo,
            IUserFeatureRepository userFeatureRepo
        ) : base()
        {
            _userRoleRepo = userRoleRepo;
            _userFeatureRepo = userFeatureRepo;
        }

        public override int Delete(User entity, string account = "system", bool persist = true)
        {
            _userFeatureRepo.DeleteRange(entity.UserFeatures, account, false);
            _userRoleRepo.DeleteRange(entity.UserRoles, account, false);

            return base.Delete(entity, account, persist);
        }

        public override async Task<int> DeleteAsync(User entity, string account = "system", bool persist = true)
        {
            await _userFeatureRepo.DeleteRangeAsync(entity.UserFeatures, account, false);
            await _userRoleRepo.DeleteRangeAsync(entity.UserRoles, account, false);

            return await base.DeleteAsync(entity, account, persist);
        }

        public override int DeleteRange(IEnumerable<User> entities, string account = "system", bool persist = true)
        {
            foreach (var entity in entities)
            {
                _userFeatureRepo.DeleteRange(entity.UserFeatures, account, false);
                _userRoleRepo.DeleteRange(entity.UserRoles, account, false);
            }

            return base.DeleteRange(entities, account, persist);
        }

        public override async Task<int> DeleteRangeAsync(IEnumerable<User> entities, string account = "system", bool persist = true)
        {
            foreach (var entity in entities)
            {
                await _userFeatureRepo.DeleteRangeAsync(entity.UserFeatures, account, false);
                await _userRoleRepo.DeleteRangeAsync(entity.UserRoles, account, false);
            }

            return await base.DeleteRangeAsync(entities, account, persist);
        }

        public IEnumerable<User> GetUsersFull()
        {
            return Table
                .Include(u => u.UserRoles)
                    .ThenInclude(ur => ur.Role)
                        .ThenInclude(r => r.RoleFeatures)
                            .ThenInclude(rf => rf.Feature)
                .Include(u => u.UserFeatures)
                    .ThenInclude(uf => uf.Feature)
                .ToList();
        }

        public async Task<IEnumerable<User>> GetUsersFullAsync()
        {
            return await Table
                .Include(u => u.UserRoles)
                    .ThenInclude(ur => ur.Role)
                        .ThenInclude(r => r.RoleFeatures)
                            .ThenInclude(rf => rf.Feature)
                .Include(u => u.UserFeatures)
                    .ThenInclude(uf => uf.Feature)
                .ToListAsync();
        }

        public User GetUserFull(string loginId)
        {
            return Table
                .Include(u => u.UserRoles)
                    .ThenInclude(ur => ur.Role)
                        .ThenInclude(r => r.RoleFeatures)
                            .ThenInclude(rf => rf.Feature)
                .Include(u => u.UserFeatures)
                    .ThenInclude(uf => uf.Feature)
                .SingleOrDefault(u => u.LoginId == loginId);
        }

        public async Task<User> GetUserFullAsync(string loginId)
        {
            return await Table
                .Include(u => u.UserRoles)
                    .ThenInclude(ur => ur.Role)
                        .ThenInclude(r => r.RoleFeatures)
                            .ThenInclude(rf => rf.Feature)
                .Include(u => u.UserFeatures)
                    .ThenInclude(uf => uf.Feature)
                .SingleOrDefaultAsync(u => u.LoginId == loginId);
        }

        public User GetUserFull(int id)
        {
            return Table
                .Include(u => u.UserRoles)
                    .ThenInclude(ur => ur.Role)
                        .ThenInclude(r => r.RoleFeatures)
                            .ThenInclude(rf => rf.Feature)
                .Include(u => u.UserFeatures)
                    .ThenInclude(uf => uf.Feature)
                .SingleOrDefault(u => u.Id == id);
        }

        public async Task<User> GetUserFullAsync(int id)
        {
            return await Table
                .Include(u => u.UserRoles)
                    .ThenInclude(ur => ur.Role)
                        .ThenInclude(r => r.RoleFeatures)
                            .ThenInclude(rf => rf.Feature)
                .Include(u => u.UserFeatures)
                    .ThenInclude(uf => uf.Feature)
                .SingleOrDefaultAsync(u => u.Id == id);
        }

        public User GetUser(string loginId)
        {
            return Table.SingleOrDefault(u => u.LoginId == loginId);
        }

        public async Task<User> GetUserAsync(string loginId)
        {
            return await Table.SingleOrDefaultAsync(u => u.LoginId == loginId);
        }

        public int AddUser(User newUser, string password, string account = "system")
        {
            using (var connection = new SqlConnection(Context.Database.GetDbConnection().ConnectionString))
            {
                using (var command = new SqlCommand("System.uspAddUser", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@LoginId", newUser.LoginId);
                    command.Parameters.AddWithValue("@Password", password);
                    command.Parameters.AddWithValue("@UserName", newUser.UserName);
                    command.Parameters.AddWithValue("@TelephoneNumbers", newUser.TelephoneNumbers);
                    command.Parameters.AddWithValue("@Email", newUser.Email);
                    command.Parameters.AddWithValue("@InsertBy", account);
                    var id = command.Parameters.Add("@Id", SqlDbType.Int);
                    id.Direction = ParameterDirection.Output;
                    var response = command.Parameters.Add("@Response", SqlDbType.NVarChar, 250);
                    response.Direction = ParameterDirection.Output;
                    var retCode = command.Parameters.Add("@ReturnCode", SqlDbType.Int);
                    retCode.Direction = ParameterDirection.ReturnValue;

                    connection.Open();
                    command.ExecuteNonQuery();

                    int result = (int)retCode.Value;

                    //if (result != 0)
                    //{
                    //    return null;
                    //}

                    //return Find((int)id.Value);

                    if (result != 0)
                    {
                        return 0;
                    }

                    return (int)id.Value;
                }
            }
        }

        public async Task<int> AddUserAsync(User newUser, string password, string account = "system")
        {
            using (var connection = new SqlConnection(Context.Database.GetDbConnection().ConnectionString))
            {
                using (var command = new SqlCommand("System.uspAddUser", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@LoginId", newUser.LoginId);
                    command.Parameters.AddWithValue("@Password", password);
                    command.Parameters.AddWithValue("@UserName", newUser.UserName);
                    command.Parameters.AddWithValue("@TelephoneNumbers", newUser.TelephoneNumbers);
                    command.Parameters.AddWithValue("@Email", newUser.Email);
                    command.Parameters.AddWithValue("@InsertBy", account);
                    var id = command.Parameters.Add("@Id", SqlDbType.Int);
                    id.Direction = ParameterDirection.Output;
                    var response = command.Parameters.Add("@Response", SqlDbType.NVarChar, 250);
                    response.Direction = ParameterDirection.Output;
                    var retCode = command.Parameters.Add("@ReturnCode", SqlDbType.Int);
                    retCode.Direction = ParameterDirection.ReturnValue;

                    await connection.OpenAsync();
                    await command.ExecuteNonQueryAsync();

                    int result = (int)retCode.Value;

                    //if (result != 0)
                    //{
                    //    return null;
                    //}

                    //return await FindAsync((int)id.Value);

                    if (result != 0)
                    {
                        return 0;
                    }

                    return (int)id.Value;
                }
            }
        }

        public int GetUserId(string loginId)
        {
            var user = Table.Where(u => u.LoginId == loginId).SingleOrDefault();

            if (user == null)
            {
                return -1;
            }

            return user.Id;
        }

        public async Task<int> GetUserIdAsync(string loginId)
        {
            var user = await Table.Where(u => u.LoginId == loginId).SingleOrDefaultAsync();

            if (user == null)
            {
                return -1;
            }

            return user.Id;
        }

        public int ChangePassword(string loginId, string oldPassword, string newPassword, string account = "system")
        {
            using (var connection = new SqlConnection(Context.Database.GetDbConnection().ConnectionString))
            {
                using (var command = new SqlCommand("System.uspChangeUserPassword", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@LoginId", loginId);
                    command.Parameters.AddWithValue("@OldPassword", oldPassword);
                    command.Parameters.AddWithValue("@NewPassword", newPassword);
                    command.Parameters.AddWithValue("@UpdateBy", account);
                    var response = command.Parameters.Add("@Response", SqlDbType.NVarChar, 250);
                    response.Direction = ParameterDirection.Output;
                    var retCode = command.Parameters.Add("@ReturnCode", SqlDbType.Int);
                    retCode.Direction = ParameterDirection.ReturnValue;

                    connection.Open();
                    command.ExecuteNonQuery();

                    return (int)retCode.Value;
                }
            }
        }

        public async Task<int> ChangePasswordAsync(string loginId, string oldPassword, string newPassword, string account = "system")
        {
            using (var connection = new SqlConnection(Context.Database.GetDbConnection().ConnectionString))
            {
                using (var command = new SqlCommand("System.uspChangeUserPassword", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@LoginId", loginId);
                    command.Parameters.AddWithValue("@OldPassword", oldPassword);
                    command.Parameters.AddWithValue("@NewPassword", newPassword);
                    command.Parameters.AddWithValue("@UpdateBy", account);
                    var response = command.Parameters.Add("@Response", SqlDbType.NVarChar, 250);
                    response.Direction = ParameterDirection.Output;
                    var retCode = command.Parameters.Add("@ReturnCode", SqlDbType.Int);
                    retCode.Direction = ParameterDirection.ReturnValue;

                    await connection.OpenAsync();
                    await command.ExecuteNonQueryAsync();

                    return (int)retCode.Value;
                }
            }
        }

        public int UpdateLogin(User user, bool persist = true)
        {
            Table.Update(user);
            return persist ? SaveChanges() : 0;
        }

        public async Task<int> UpdateLoginAsync(User user, bool persist = true)
        {
            Table.Update(user);
            return persist ? await SaveChangesAsync() : 0;
        }
    }
}
