﻿using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.Base;

namespace QXAccounting.DAL.Repositories.interfaces
{
    public interface ICurrencyRepository : IRepository<Currency>
    {
    }
}
