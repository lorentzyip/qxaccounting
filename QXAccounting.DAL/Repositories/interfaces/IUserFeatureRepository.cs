﻿using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.Base;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QXAccounting.DAL.Repositories.interfaces
{
    public interface IUserFeatureRepository : IRepository<UserFeature>
    {
        // pass user id or loginid to get features
        IEnumerable<Feature> GetFeatures(int id);
        IEnumerable<Feature> GetFeatures(string loginId);
        Task<IEnumerable<Feature>> GetFeaturesAsync(int id);
        Task<IEnumerable<Feature>> GetFeaturesAsync(string loginId);

        IEnumerable<UserFeature> UpdateUserFeatures(int userId, IEnumerable<int> featureIds, string account = "system", bool persist = true);
        Task<IEnumerable<UserFeature>> UpdateUserFeaturesAsync(int userId, IEnumerable<int> featureIds, string account = "system", bool persist = true);
    }
}
