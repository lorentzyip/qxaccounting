﻿using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.Base;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace QXAccounting.DAL.Repositories.interfaces
{
    public interface IWorkRepository : IRepository<Work>
    {
        // Create closing history and exchange history entries
        int close(string account = "system");

        int UpdateWork(Work work, string account = "system", bool persist = true);
        Task<int> UpdateWorkAsync(Work work, string account = "system", bool persist = true);

        IEnumerable<Work> GetAllWorks();
        Task<IEnumerable<Work>> GetAllWorksAsync();

        Work GetWork(int id);
        Task<Work> GetWorkAsync(int id);
    }
}
