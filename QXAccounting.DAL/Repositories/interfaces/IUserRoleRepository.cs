﻿using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.Base;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QXAccounting.DAL.Repositories.interfaces
{
    public interface IUserRoleRepository : IRepository<UserRole>
    {
        // pass user id or loginid to get roles
        IEnumerable<Role> GetRoles(int id);
        IEnumerable<Role> GetRoles(string loginId);
        Task<IEnumerable<Role>> GetRolesAsync(int id);
        Task<IEnumerable<Role>> GetRolesAsync(string loginId);

        IEnumerable<UserRole> UpdateUserRoles(int userId, IEnumerable<int> roleIds, string account = "system", bool persist = true);
        Task<IEnumerable<UserRole>> UpdateUserRolesAsync(int userId, IEnumerable<int> roleIds, string account = "system", bool persist = true);
    }
}
