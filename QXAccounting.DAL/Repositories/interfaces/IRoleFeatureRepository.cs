﻿using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QXAccounting.DAL.Repositories.interfaces
{
    public interface IRoleFeatureRepository : IRepository<RoleFeature>
    {
        IEnumerable<Feature> GetFeatures(int roleId);
        Task<IEnumerable<Feature>> GetFeaturesAsync(int roleId);

        IEnumerable<RoleFeature> UpdateRoleFeatures(int roleId, IEnumerable<int> featureIds, string account = "system", bool persist = true);
        Task<IEnumerable<RoleFeature>> UpdateRoleFeaturesAsync(int roleId, IEnumerable<int> featureIds, string account = "system", bool persist = true);
    }
}
