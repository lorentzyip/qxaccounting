﻿using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QXAccounting.DAL.Repositories.interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        IEnumerable<User> GetUsersFull();
        Task<IEnumerable<User>> GetUsersFullAsync();

        User GetUserFull(string loginId);
        Task<User> GetUserFullAsync(string loginId);

        User GetUserFull(int id);
        Task<User> GetUserFullAsync(int id);

        User GetUser(string loginId);
        Task<User> GetUserAsync(string loginId);

        // return new user id if success, 0 otherwise
        int AddUser(User newUser, string password, string account = "system");
        Task<int> AddUserAsync(User newUser, string password, string account = "system");

        int ChangePassword(string loginId, string oldPassword, string newPassword, string account = "system");
        Task<int> ChangePasswordAsync(string loginId, string oldPassword, string newPassword, string account = "system");

        int GetUserId(string loginId);
        Task<int> GetUserIdAsync(string loginId);

        int UpdateLogin(User user, bool persist = true);
        Task<int> UpdateLoginAsync(User user, bool persist = true);
    }
}
