﻿using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.Base;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QXAccounting.DAL.Repositories.interfaces
{
    public interface ICompanyRepository : IRepository<Company>
    {
        IEnumerable<Company> GetAllCompanies();
        Task<IEnumerable<Company>> GetAllCompaniesAsync();

        Company GetCompany(int id);
        Task<Company> GetCompanyAsync(int id);

        IEnumerable<Shareholder> GetShareholders(int id);
        Task<IEnumerable<Shareholder>> GetShareholdersAsync(int id);

        int AddShareholder(int companyId, Shareholder shareholder, string account = "system", bool persist = true);
        Task<int> AddShareholderAsync(int companyId, Shareholder shareholder, string account = "system", bool persist = true);
        int AddShareholder(Company company, Shareholder shareholder, string account = "system", bool persist = true);
        Task<int> AddShareholderAsync(Company company, Shareholder shareholder, string account = "system", bool persist = true);

        //int AddCompany(Company company, string account = "system", bool persist = true);
        //Task<int> AddCompanyAsync(Company company, string account = "system", bool persist = true);

        //int DeleteShareholder(int companyId, Shareholder shareholder, string account = "system", bool persist = true);
        //Task<int> DeleteShareholderAsync(int companyId, Shareholder shareholder, string account = "system", bool persist = true);
        //int DeleteShareholder(Company company, Shareholder shareholder, string account = "system", bool persist = true);
        //Task<int> DeleteShareholderAsync(Company company, Shareholder shareholder, string account = "system", bool persist = true);
    }
}
