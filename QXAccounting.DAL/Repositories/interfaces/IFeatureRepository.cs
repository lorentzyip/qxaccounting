﻿using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QXAccounting.DAL.Repositories.interfaces
{
    public interface IFeatureRepository : IRepository<Feature>
    {
        IEnumerable<Feature> GetFeaturesFull();
        Task<IEnumerable<Feature>> GetFeaturesFullAsync();

        Feature GetFeatureFull(string featureCode);
        Task<Feature> GetFeatureFullAsync(string featureCode);

        Feature GetFeatureFull(int id);
        Task<Feature> GetFeatureFullAsync(int id);

        Feature GetFeature(string featureCode);
        Task<Feature> GetFeatureAsync(string featureCode);
    }
}
