﻿using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.Base;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QXAccounting.DAL.Repositories.interfaces
{
    public interface IShareholderRepository : IRepository<Shareholder>
    {
        IEnumerable<Shareholder> GetShareholdersByCompanyId(int companyId);
        Task<IEnumerable<Shareholder>> GetShareholdersByCompanyIdAsync(int companyId);
    }
}
