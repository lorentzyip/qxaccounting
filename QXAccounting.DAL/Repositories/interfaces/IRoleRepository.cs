﻿using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.Base;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QXAccounting.DAL.Repositories.interfaces
{
    public interface IRoleRepository : IRepository<Role>
    {
        IEnumerable<Role> GetRolesFull();
        Task<IEnumerable<Role>> GetRolesFullAsync();

        Role GetRoleFull(int id);
        Task<Role> GetRoleFullAsync(int id);
    }
}
