﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using QXAccounting.DAL.Repositories.Base;
using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.DAL.EF;

namespace QXAccounting.DAL.Repositories
{
    public class UserRoleRepository : RepositoryBase<UserRole>, IUserRoleRepository
    {
        public UserRoleRepository(
            DbContextOptions<QXAccountingContext> options
        ) : base(options)
        {
        }

        public UserRoleRepository() : base()
        { }

        public IEnumerable<Role> GetRoles(int id)
        {
            var roles = Table
                        .Where(ur => ur.UserId == id)
                        .Include(ur => ur.Role)
                        .Select(ur => ur.Role)
                        .ToList();

            return roles;
        }

        public IEnumerable<Role> GetRoles(string loginId)
        {
            var user = Context.Users
                .AsNoTracking()
                .SingleOrDefault(u => u.LoginId == loginId);

            if (user == null)
            {
                return null;
            }

            return GetRoles(user.Id);
        }

        public async Task<IEnumerable<Role>> GetRolesAsync(int id)
        {
            var roles = await Table
                        .Where(ur => ur.UserId == id)
                        .Include(ur => ur.Role)
                        .Select(ur => ur.Role)
                        .ToListAsync();

            return roles;
        }

        public async Task<IEnumerable<Role>> GetRolesAsync(string loginId)
        {
            var user = await Context.Users
                .AsNoTracking()
                .SingleOrDefaultAsync(u => u.LoginId == loginId);

            if (user == null)
            {
                return null;
            }

            return await GetRolesAsync(user.Id);
        }

        public IEnumerable<UserRole> UpdateUserRoles(int userId, IEnumerable<int> roleIds, string account = "system", bool persist = true)
        {
            var userRolesToUpdate = Table.Where(ur => ur.UserId == userId).OrderBy(ur => ur.RoleId).ToList();

            if (roleIds == null || roleIds?.Count() == 0)
            {
                base.DeleteRange(userRolesToUpdate, account, persist);
            } else
            {
                var existingRolesIds = userRolesToUpdate.Select(rf => rf.RoleId);

                var roleIdsToDelete = existingRolesIds.Except(roleIds);
                var roleIdsToAdd = roleIds.Except(existingRolesIds);
                var roleIdsToEnable = existingRolesIds.Intersect(roleIds);

                var userRolesToDelete = userRolesToUpdate.Where(ur => roleIdsToDelete.Contains(ur.RoleId));
                var userRolesToEnable = userRolesToUpdate.Where(ur => roleIdsToEnable.Contains(ur.RoleId));

                base.DeleteRange(userRolesToDelete, account, false);

                foreach (var userRole in userRolesToEnable)
                {
                    userRole.Deleted = false;
                    userRole.DeleteBy = null;
                    userRole.DeleteTime = null;
                }
                base.UpdateRange(userRolesToEnable, account, false);

                foreach (var roleId in roleIdsToAdd)
                {
                    base.Add(new UserRole { UserId = userId, RoleId = roleId }, account, false);
                }

                SaveChanges();
            }

            return userRolesToUpdate;
        }

        public async Task<IEnumerable<UserRole>> UpdateUserRolesAsync(int userId, IEnumerable<int> roleIds, string account = "system", bool persist = true)
        {
            var userRolesToUpdate = Table.Where(ur => ur.UserId == userId).OrderBy(ur => ur.RoleId).ToList();

            if (roleIds == null || roleIds?.Count() == 0)
            {
                await base.DeleteRangeAsync(userRolesToUpdate, account, persist);
            }
            else
            {
                var existingRolesIds = userRolesToUpdate.Select(rf => rf.RoleId);

                var roleIdsToDelete = existingRolesIds.Except(roleIds);
                var roleIdsToAdd = roleIds.Except(existingRolesIds);
                var roleIdsToEnable = existingRolesIds.Intersect(roleIds);

                var userRolesToDelete = userRolesToUpdate.Where(ur => roleIdsToDelete.Contains(ur.RoleId));
                var userRolesToEnable = userRolesToUpdate.Where(ur => roleIdsToEnable.Contains(ur.RoleId));

                await base.DeleteRangeAsync(userRolesToDelete, account, false);

                foreach (var userRole in userRolesToEnable)
                {
                    userRole.Deleted = false;
                    userRole.DeleteBy = null;
                    userRole.DeleteTime = null;
                }
                await base.UpdateRangeAsync(userRolesToEnable, account, false);

                foreach (var roleId in roleIdsToAdd)
                {
                    await base.AddAsync(new UserRole { UserId = userId, RoleId = roleId }, account, false);
                }

                await SaveChangesAsync();
            }

            return userRolesToUpdate;
        }
    }
}
