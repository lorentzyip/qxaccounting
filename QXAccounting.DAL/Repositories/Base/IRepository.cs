﻿using QXAccounting.DAL.EF;
using QXAccounting.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QXAccounting.DAL.Repositories.Base
{
    public interface IRepository<T> where T : EntityBase
    {
        QXAccountingContext Context { get; }

        //int Count { get; }
        int Count();
        Task<int> CountAsync();

        bool HasChanges { get; }

        T Find(int? id);
        Task<T> FindAsync(int? id);

        T GetFirst();
        Task<T> GetFirstAsync();

        IEnumerable<T> GetAll();
        Task<IEnumerable<T>> GetAllAsync();

        IEnumerable<T> GetRange(int skip, int take);

        int Add(T entity, string account = "system", bool persist = true);
        Task<int> AddAsync(T entity, string account = "system", bool persist = true);
        int AddRange(IEnumerable<T> entities, string account = "system", bool persist = true);
        Task<int> AddRangeAsync(IEnumerable<T> entities, string account = "system", bool persist = true);

        int Update(T entity, string account = "system", bool persist = true);
        Task<int> UpdateAsync(T entity, string account = "system", bool persist = true);
        int UpdateRange(IEnumerable<T> entities, string account = "system", bool persist = true);
        Task<int> UpdateRangeAsync(IEnumerable<T> entities, string account = "system", bool persist = true);

        //int Delete(int id, string account = "system", bool persist = true);
        //Task<int> DeleteAsync(int id, string account = "system", bool persist = true);
        int Delete(T entity, string account = "system", bool persist = true);
        Task<int> DeleteAsync(T entity, string account = "system", bool persist = true);
        int DeleteRange(IEnumerable<T> entities, string account = "system", bool persist = true);
        Task<int> DeleteRangeAsync(IEnumerable<T> entities, string account = "system", bool persist = true);

        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
