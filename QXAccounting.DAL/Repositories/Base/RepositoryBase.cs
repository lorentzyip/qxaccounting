﻿using Microsoft.EntityFrameworkCore;
using QXAccounting.DAL.EF;
using QXAccounting.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QXAccounting.DAL.Repositories.Base
{
    public abstract class RepositoryBase<T> : IDisposable, IRepository<T> where T : EntityBase, new()
    {
        protected readonly QXAccountingContext Db;
        protected DbSet<T> Table;

        protected RepositoryBase()
        {
            Db = new QXAccountingContext();
            Table = Db.Set<T>();
        }

        protected RepositoryBase(DbContextOptions<QXAccountingContext> options)
        {
            Db = new QXAccountingContext(options);
            Table = Db.Set<T>();
        }

        public QXAccountingContext Context => Db;

        public bool HasChanges => Db.ChangeTracker.HasChanges();

        public int Count() => Table.Count();

        public async Task<int> CountAsync() => await Table.CountAsync();

        public T GetFirst() => Table.FirstOrDefault();

        public async Task<T> GetFirstAsync() => await Table.FirstOrDefaultAsync();

        public T Find(int? id) => Table.Find(id);

        public async Task<T> FindAsync(int? id) => await Table.FindAsync(id);

        public virtual IEnumerable<T> GetAll() => Table.ToList();

        public virtual async Task<IEnumerable<T>> GetAllAsync() => await Table.ToListAsync();

        internal IEnumerable<T> GetRange(IQueryable<T> query, int skip, int take)
            => query.Skip(skip).Take(take);
        public virtual IEnumerable<T> GetRange(int skip, int take)
            => GetRange(Table, skip, take);

        public virtual int Add(T entity, string account = "system", bool persist = true)
        {
            entity.InsertBy = account;
            entity.InsertTime = DateTime.Now;

            Table.Add(entity);
            return persist ? SaveChanges() : 0;
        }

        public virtual async Task<int> AddAsync(T entity, string account = "system", bool persist = true)
        {
            entity.InsertBy = account;
            entity.InsertTime = DateTime.Now;

            await Table.AddAsync(entity);
            return persist ? await SaveChangesAsync() : 0;
        }

        public virtual int AddRange(IEnumerable<T> entities, string account = "system", bool persist = true)
        {
            foreach (T entity in entities)
            {
                entity.InsertBy = account;
                entity.InsertTime = DateTime.Now;
            }

            Table.AddRange(entities);
            return persist ? SaveChanges() : 0;
        }

        public virtual async Task<int> AddRangeAsync(IEnumerable<T> entities, string account = "system", bool persist = true)
        {
            foreach (T entity in entities)
            {
                entity.InsertBy = account;
                entity.InsertTime = DateTime.Now;
            }

            await Table.AddRangeAsync(entities);
            return persist ? await SaveChangesAsync() : 0;
        }

        public virtual int Update(T entity, string account = "system", bool persist = true)
        {
            entity.UpdateBy = account;
            entity.UpdateTime = DateTime.Now;

            Table.Update(entity);
            return persist ? SaveChanges() : 0;
        }

        public virtual async Task<int> UpdateAsync(T entity, string account = "system", bool persist = true)
        {
            entity.UpdateBy = account;
            entity.UpdateTime = DateTime.Now;

            Table.Update(entity);
            return persist ? await SaveChangesAsync() : 0;
        }

        public virtual int UpdateRange(IEnumerable<T> entities, string account = "system", bool persist = true)
        {
            foreach (T entity in entities)
            {
                entity.UpdateBy = account;
                entity.UpdateTime = DateTime.Now;
            }

            Table.UpdateRange(entities);
            return persist ? SaveChanges() : 0;
        }

        public virtual async Task<int> UpdateRangeAsync(IEnumerable<T> entities, string account = "system", bool persist = true)
        {
            foreach (T entity in entities)
            {
                entity.UpdateBy = account;
                entity.UpdateTime = DateTime.Now;
            }

            Table.UpdateRange(entities);
            return persist ? await SaveChangesAsync() : 0;
        }

        public virtual int Delete(T entity, string account = "system", bool persist = true)
        {
            // should handle different delete action: turn on the delete flag
            //Table.Remove(entity);

            entity.DeleteBy = account;
            entity.DeleteTime = DateTime.Now;
            entity.Deleted = true;

            Table.Update(entity);

            return persist ? SaveChanges() : 0;
        }

        //public virtual int Delete(int id, string account = "system", bool persist = true)
        //{
        //    T entity = Table.Find(id);

        //    return Delete(entity, account, persist);
        //}

        public virtual async Task<int> DeleteAsync(T entity, string account = "system", bool persist = true)
        {
            // should handle different delete action: turn on the delete flag
            //Table.Remove(entity);

            entity.DeleteBy = account;
            entity.DeleteTime = DateTime.Now;
            entity.Deleted = true;

            Table.Update(entity);

            return persist ? await SaveChangesAsync() : 0;
        }

        //public virtual async Task<int> DeleteAsync(int id, string account = "system", bool persist = true)
        //{
        //    T entity = await Table.FindAsync(id);

        //    return await DeleteAsync(entity, account, persist);
        //}

        public virtual int DeleteRange(IEnumerable<T> entities, string account = "system", bool persist = true)
        {
            // should handle different delete action: turn on the delete flag
            // Table.RemoveRange(entities);

            foreach (T entity in entities)
            {
                entity.DeleteBy = account;
                entity.DeleteTime = DateTime.Now;
                entity.Deleted = true;
            }

            Table.UpdateRange(entities);

            return persist ? SaveChanges() : 0;
        }

        public virtual async Task<int> DeleteRangeAsync(IEnumerable<T> entities, string account = "system", bool persist = true)
        {
            // should handle different delete action: turn on the delete flag
            // Table.RemoveRange(entities);

            foreach (T entity in entities)
            {
                entity.DeleteBy = account;
                entity.DeleteTime = DateTime.Now;
                entity.Deleted = true;
            }

            Table.UpdateRange(entities);

            return persist ? await SaveChangesAsync() : 0;
        }

        public int SaveChanges()
        {
            try
            {
                return Db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                // Should log
                Console.WriteLine(ex);
                throw;
            }
            catch (Exception ex)
            {
                // Should log
                Console.WriteLine(ex);
                throw;
            }
        }

        public async Task<int> SaveChangesAsync()
        {
            try
            {
                return await Db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                // Should log
                Console.WriteLine(ex);
                throw;
            }
            catch (Exception ex)
            {
                // Should log
                Console.WriteLine(ex);
                throw;
            }
        }

        bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
            }
            Db.Dispose();
            _disposed = true;
        }
    }
}
