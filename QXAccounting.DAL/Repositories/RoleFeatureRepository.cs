﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using QXAccounting.DAL.Repositories.Base;
using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.DAL.EF;

namespace QXAccounting.DAL.Repositories
{
    public class RoleFeatureRepository : RepositoryBase<RoleFeature>, IRoleFeatureRepository
    {
        public RoleFeatureRepository(DbContextOptions<QXAccountingContext> options) : base(options)
        { }

        public RoleFeatureRepository() : base()
        { }

        public IEnumerable<Feature> GetFeatures(int roleId)
        {
            var features = Table
                .Where(rf => rf.RoleId == roleId && !rf.Deleted)
                .Include(rf => rf.Feature)
                .Select(rf => rf.Feature)
                .ToList();

            return features;
        }

        public async Task<IEnumerable<Feature>> GetFeaturesAsync(int roleId)
        {
            var features = await Table
                .Where(rf => rf.RoleId == roleId && !rf.Deleted)
                .Include(rf => rf.Feature)
                .Select(rf => rf.Feature)
                .ToListAsync();

            return features;
        }

        public IEnumerable<RoleFeature> UpdateRoleFeatures(int roleId, IEnumerable<int> featureIds, string account, bool persist)
        {
            var roleFeaturesToUpdate = Table.Where(rf => rf.RoleId == roleId).ToList();

            if (featureIds == null || featureIds?.Count() == 0)
            {
                base.DeleteRange(roleFeaturesToUpdate, account, persist);
            }
            else
            {
                var existingFeatureIds = roleFeaturesToUpdate.Select(rf => rf.FeatureId);
                //var existingFeatureIds = roleToUpdate.RoleFeatures.Select(rf => rf.FeatureId);

                var featureIdsToDelete = existingFeatureIds.Except(featureIds);
                var featureIdsToAdd = featureIds.Except(existingFeatureIds);
                var featureIdsToEnable = existingFeatureIds.Intersect(featureIds);

                var roleFeaturesToDelete = roleFeaturesToUpdate.Where(rf => featureIdsToDelete.Contains(rf.FeatureId));
                var roleFeaturesToEnable = roleFeaturesToUpdate.Where(rf => featureIdsToEnable.Contains(rf.FeatureId));

                base.DeleteRange(roleFeaturesToDelete, account, false);

                foreach (var roleFeature in roleFeaturesToEnable)
                {
                    roleFeature.Deleted = false;
                    roleFeature.DeleteBy = null;
                    roleFeature.DeleteTime = null;
                }
                base.UpdateRange(roleFeaturesToEnable, account, false);

                foreach (var featureId in featureIdsToAdd)
                {
                    base.Add(new RoleFeature { RoleId = roleId, FeatureId = featureId }, account, false);
                }

                SaveChanges();
            }

            return roleFeaturesToUpdate;
        }

        public async Task<IEnumerable<RoleFeature>> UpdateRoleFeaturesAsync(int roleId, IEnumerable<int> featureIds, string account, bool persist)
        {
            var roleFeaturesToUpdate = Table.Where(rf => rf.RoleId == roleId).ToList();

            if (featureIds == null || featureIds?.Count() == 0)
            {
                await base.DeleteRangeAsync(roleFeaturesToUpdate, account, persist);
            }
            else
            {
                var existingFeatureIds = roleFeaturesToUpdate.Select(rf => rf.FeatureId);
                //var existingFeatureIds = roleToUpdate.RoleFeatures.Select(rf => rf.FeatureId);

                var featureIdsToDelete = existingFeatureIds.Except(featureIds);
                var featureIdsToAdd = featureIds.Except(existingFeatureIds);
                var featureIdsToEnable = existingFeatureIds.Intersect(featureIds);

                var roleFeaturesToDelete = roleFeaturesToUpdate.Where(rf => featureIdsToDelete.Contains(rf.FeatureId));
                var roleFeaturesToEnable = roleFeaturesToUpdate.Where(rf => featureIdsToEnable.Contains(rf.FeatureId));

                await base.DeleteRangeAsync(roleFeaturesToDelete, account, false);

                foreach (var roleFeature in roleFeaturesToEnable)
                {
                    roleFeature.Deleted = false;
                    roleFeature.DeleteBy = null;
                    roleFeature.DeleteTime = null;
                }
                await base.UpdateRangeAsync(roleFeaturesToEnable, account, false);

                foreach (var featureId in featureIdsToAdd)
                {
                    await base.AddAsync(new RoleFeature { RoleId = roleId, FeatureId = featureId }, account, false);
                }

                await SaveChangesAsync();
            }

            return roleFeaturesToUpdate;
        }
    }
}
