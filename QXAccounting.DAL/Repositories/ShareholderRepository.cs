﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using QXAccounting.DAL.EF;
using QXAccounting.DAL.Repositories.Base;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.Models.Entities;
using System.Linq;

namespace QXAccounting.DAL.Repositories
{
    public class ShareholderRepository : RepositoryBase<Shareholder>, IShareholderRepository
    {
        public ShareholderRepository(DbContextOptions<QXAccountingContext> options) : base(options)
        { }

        public ShareholderRepository() : base()
        { }

        public IEnumerable<Shareholder> GetShareholdersByCompanyId(int companyId)
        {
            var shareholders = Context.Shareholders.Where(sh => sh.CompanyId == companyId).ToList();
            return shareholders;
        }

        public async Task<IEnumerable<Shareholder>> GetShareholdersByCompanyIdAsync(int companyId)
        {
            var shareholders = await Context.Shareholders.Where(sh => sh.CompanyId == companyId).ToListAsync();

            return shareholders;
        }
    }
}
