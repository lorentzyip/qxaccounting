﻿using Microsoft.EntityFrameworkCore;
using QXAccounting.Models.Entities;
using QXAccounting.DAL.Repositories.Base;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.DAL.EF;

namespace QXAccounting.DAL.Repositories
{
    public class ClosingHistoryRepository : RepositoryBase<ClosingHistory>, IClosingHistoryRepository
    {
        public ClosingHistoryRepository(DbContextOptions<QXAccountingContext> options) : base(options)
        { }

        public ClosingHistoryRepository() : base()
        { }
    }
}
