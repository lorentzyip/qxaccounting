﻿using AutoMapper;
using QXAccounting.Models.Entities;
using QXAccounting.WebApi.ViewModels;

namespace QXAccounting.WebApi
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserVM>().ReverseMap();
            CreateMap<User, NewUserVM>().ReverseMap();
            CreateMap<Role, RoleVM>().ReverseMap();
            CreateMap<Feature, FeatureVM>().ReverseMap();
            CreateMap<UserRole, UserRoleVM>().ReverseMap();
            CreateMap<Company, CompanyVM>().ReverseMap();
            CreateMap<Shareholder, ShareholderVM>().ReverseMap();
            CreateMap<Work, WorkVM>().ReverseMap();
        }
    }
}
