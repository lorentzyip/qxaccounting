﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QXAccounting.WebApi.ViewModels
{
    public class RoleVM
    {
        [Required]
        [MaxLength(50)]
        public string RoleName { get; set; }

        //public ICollection<RoleFeature> RoleFeatures { get; set; }
        //public ICollection<UserRole> UserRoles { get; set; }
    }
}
