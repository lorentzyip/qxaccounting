﻿using System;

namespace QXAccounting.WebApi.ViewModels
{
    public class LoginVM
    {
        public string LoginId { get; set; }
        public string Password { get; set; }
        public bool IsValid
        {
            get { return !String.IsNullOrEmpty(LoginId); }
        }
    }
}
