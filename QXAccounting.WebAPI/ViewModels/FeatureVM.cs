﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QXAccounting.WebApi.ViewModels
{
    public class FeatureVM
    {
        [Required]
        [MaxLength(50)]
        public string FeatureName { get; set; }
        [Required]
        [MaxLength(10)]
        public string FeatureCode { get; set; }

        //public ICollection<RoleFeature> RoleFeatures { get; set; }
        //public ICollection<UserFeature> UserFeatures { get; set; }
    }
}
