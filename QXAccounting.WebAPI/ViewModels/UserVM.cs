﻿using QXAccounting.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QXAccounting.WebApi.ViewModels
{
    public class UserVM : EntityBase
    {
        [MaxLength(20)]
        public string LoginId { get; set; }
        [MaxLength(50)]
        public string UserName { get; set; }
        [DataType(DataType.Password), MaxLength(20)]
        public string Password { get; set; }
        [DataType(DataType.PhoneNumber), MaxLength(100)]
        public string TelephoneNumbers { get; set; }
        [DataType(DataType.EmailAddress), MaxLength(50)]
        public string Email { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public byte[] LastLoginAddress { get; set; }

        public ICollection<UserFeature> UserFeatures { get; set; }
        public ICollection<UserRole> UserRoles { get; set; }
    }
}
