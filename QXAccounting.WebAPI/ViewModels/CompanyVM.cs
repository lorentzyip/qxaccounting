﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace QXAccounting.WebApi.ViewModels
{
    public class CompanyVM
    {
        [Required]
        [MaxLength(50)]
        public string CompanyName { get; set; }

        [Column(TypeName = "decimal(12,4)")]
        public decimal Capital { get; set; }

        [MaxLength(300)]
        public string Remarks { get; set; }

        public ICollection<ShareholderVM> Shareholders { get; set; } = new HashSet<ShareholderVM>();
    }
}
