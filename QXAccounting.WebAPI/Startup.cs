﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Hangfire;
using AutoMapper;
using QXAccounting.WebApi.Options;
using QXAccounting.DAL.EF;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.DAL.Repositories;
using UTAccounting.WebApi.Jobs;
using QXAccounting.DAL.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace QXAccounting.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.AddLogging();

            services.AddCors(options =>
                options.AddPolicy("AllowAll", p => p.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().AllowCredentials())
            );

            var jwtAppSettingsOptions = Configuration.GetSection("JwtIssuerOptions");
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["SecretJWTKey"]));
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingsOptions["Issuer"];
                options.Audience = jwtAppSettingsOptions["Audience"];
                options.ValidFor = TimeSpan.FromMinutes(Convert.ToDouble(jwtAppSettingsOptions["ValidFor"]));
                options.SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
            });
            var tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,
                // Validate the JWT Issuer claim
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingsOptions["Issuer"],
                // Validate the JWT Audience claim
                ValidateAudience = true,
                ValidAudience = jwtAppSettingsOptions["Audience"],
                // Validate the token expiry
                ValidateLifetime = true,
                RequireExpirationTime = true,

                ClockSkew = TimeSpan.Zero
            };
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(o =>
            {
                o.TokenValidationParameters = tokenValidationParameters;
            });

            services.AddDbContext<QXAccountingContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddHangfire(config => config.UseSqlServerStorage(Configuration.GetConnectionString("HangFire")));

            // Add framework services.
            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                //config.Filters.Add(new AuthorizeFilter(policy));
            })
            .AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            services.AddAutoMapper();

            // System
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IFeatureRepository, FeatureRepository>();
            services.AddScoped<IUserRoleRepository, UserRoleRepository>();
            services.AddScoped<IRoleFeatureRepository, RoleFeatureRepository>();
            services.AddScoped<IUserFeatureRepository, UserFeatureRepository>();

            // Accounting
            services.AddScoped<ICompanyRepository, CompanyRepository>();
            services.AddScoped<IShareholderRepository, ShareholderRepository>();
            services.AddScoped<ICurrencyRepository, CurrencyRepository>();
            services.AddScoped<IWorkRepository, WorkRepository>();
            services.AddScoped<IClosingHistoryRepository, ClosingHistoryRepository>();
            services.AddScoped<IExchangeHistoryRepository, ExchangeHistoryRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, QXAccountingContext context)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseCors("AllowAll");

            app.UseAuthentication();

            app.UseHangfireServer();
            app.UseHangfireDashboard("/dashboard");
            //Add jobs here. e.g. export excel, pdf, etc...
            RecurringJob.AddOrUpdate(() => ExcelJobs.SaveToExcel(env.WebRootPath), Cron.Hourly);

            app.UseMvc();

            if (env.IsDevelopment())
            {
                DbInitializer.Initialize(context);
            }
        }
    }
}
