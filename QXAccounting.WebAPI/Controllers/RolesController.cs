﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.Models.Entities;
using QXAccounting.WebApi.Helpers;
using QXAccounting.WebApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QXAccounting.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Roles")]
    [Authorize]
    public class RolesController : Controller
    {
        private IRoleRepository _roleRepo;
        private IUserRoleRepository _userRoleRepo;
        private ILogger<RolesController> _logger;
        private IMapper _mapper;

        public RolesController(
            IRoleRepository roleRepo,
            IUserRoleRepository userRoleRepo,
            ILogger<RolesController> logger,
            IMapper mapper
        )
        {
            _roleRepo = roleRepo;
            _userRoleRepo = userRoleRepo;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var roles = await _roleRepo.GetAllAsync();

            return Ok(roles);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var role = await _roleRepo.FindAsync(id);

            if (role == null)
            {
                return NotFound();
            }

            return Ok(role);
        }

        [Authorize(Roles = "Administrator,Accountant")]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] RoleVM newRole)
        {
            if (newRole == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            var role = _mapper.Map<RoleVM, Role>(newRole);

            await _roleRepo.AddAsync(role, Utils.GetClaim(User.Claims, "login_id"));

            return CreatedAtAction("Get", new { id = role.Id }, role);
        }

        [Authorize(Roles = "Administrator,Accountant")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Role role)
        {
            if (id <= 0 || role == null || id != role.Id)
            {
                return BadRequest();
            }

            await _roleRepo.UpdateAsync(role, Utils.GetClaim(User.Claims, "login_id"));

            return CreatedAtAction("Get", new { id = role.Id }, role);
        }

        [Authorize(Roles = "Administrator,Accountant")]
        [HttpPut("UpdateUserRoles/{userId}")]
        public async Task<IActionResult> UpdateUserRoles(int userId, [FromBody] IEnumerable<int> roleIds)
        {
            if (userId <= 0 || roleIds == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            var userRoles = await _userRoleRepo.UpdateUserRolesAsync(userId, roleIds, Utils.GetClaim(User.Claims, "login_id"));

            return Ok(userRoles);
        }

        [Authorize(Roles = "Administrator,Accountant")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var role = await _roleRepo.GetRoleFullAsync(id);

            if (role == null)
            {
                return NotFound();
            }

            await _roleRepo.DeleteAsync(role, Utils.GetClaim(User.Claims, "login_id"));
            return NoContent();
        }
    }
}
