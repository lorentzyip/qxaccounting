using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QXAccounting.DAL.Repositories.interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace QXAccounting.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Shareholders")]
    [Authorize]
    public class ShareholdersController : Controller
    {
        private readonly IShareholderRepository _shareholderRepo;
        private readonly IMapper _mapper;

        public ShareholdersController(
            IShareholderRepository shareholderRepo,
            IMapper mapper
        )
        {
            _shareholderRepo = shareholderRepo;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var shareholders = await _shareholderRepo.GetAllAsync();

            return Ok(shareholders);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var shareholder = await _shareholderRepo.FindAsync(id);

            return Ok(shareholder);
        }
    }
}