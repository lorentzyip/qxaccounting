using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QXAccounting.DAL.Repositories.interfaces;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using Microsoft.Extensions.Logging;
using QXAccounting.WebApi.ViewModels;
using QXAccounting.Models.Entities;
using QXAccounting.WebApi.Helpers;

namespace QXAccounting.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Works")]
    //[Authorize]
    public class WorksController : Controller
    {
        private readonly IWorkRepository _workRepo;
        private readonly IMapper _mapper;
        private readonly ILogger<WorksController> _logger;

        public WorksController(
            IWorkRepository workRepo,
            ILogger<WorksController> logger,
            IMapper mapper
        )
        {
            _workRepo = workRepo;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _workRepo.GetAllWorksAsync());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var work = await _workRepo.GetWorkAsync(id);

            if (work == null)
            {
                return NotFound();
            }

            return Ok(work);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Work work)
        {
            if (work == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            await _workRepo.AddAsync(work, Utils.GetClaim(User.Claims, "login_id"));

            return CreatedAtRoute("Get", new { id = work.Id }, work);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Work work)
        {
            if (id <= 0 || work == null || id != work.Id)
            {
                return BadRequest();
            }

            await _workRepo.UpdateWorkAsync(work, Utils.GetClaim(User.Claims, "login_id"));

            return NoContent();
        }

        //[HttpPut]
        //public async Task<IActionResult> Close(int id, [FromBody] WorkVM work)
        //{
        //}
    }
}