﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QXAccounting.DAL.Repositories;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.Models.Entities;
using QXAccounting.WebApi.Helpers;
using QXAccounting.WebApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QXAccounting.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Companies")]
    [Authorize]
    public class CompaniesController : Controller
    {
        private readonly ICompanyRepository _companyRepo;
        private readonly IMapper _mapper;

        public CompaniesController(
            ICompanyRepository companyRepo,
            IMapper mapper
        )
        {
            _companyRepo = companyRepo;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var companies = await _companyRepo.GetAllCompaniesAsync();

            return Ok(companies);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var company = await _companyRepo.GetCompanyAsync(id);

            if (company == null)
            {
                return NotFound();
            }

            return Ok(company);
        }

        [HttpGet("GetShareHolders/{id}")]
        public async Task<IActionResult> GetShareholders(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var shareholders = await _companyRepo.GetShareholdersAsync(id);

            if (shareholders == null)
            {
                return NotFound();
            }

            return Ok(shareholders);
        }

        [Authorize(Roles = "Administrator,Accountant")]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CompanyVM newCompany)
        {
            if (newCompany == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            var company = _mapper.Map<CompanyVM, Company>(newCompany);

            await _companyRepo.AddAsync(company, Utils.GetClaim(User.Claims, "login_id"));

            return CreatedAtAction("Get", new { id = company.Id }, company);
        }

        [Authorize(Roles = "Administrator,Accountant")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Company company)
        {
            if (company == null || id != company.Id)
            {
                return BadRequest();
            }

            await _companyRepo.UpdateAsync(company, Utils.GetClaim(User.Claims, "login_id"));

            return NoContent();
        }

        [Authorize(Roles = "Administrator,Accountant")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var company = await _companyRepo.GetCompanyAsync(id);

            if (company == null)
            {
                return NotFound();
            }

            await _companyRepo.DeleteAsync(company, Utils.GetClaim(User.Claims, "login_id"));

            return NoContent();
        }
    }
}
