using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.WebApi.ViewModels;
using QXAccounting.Models.Entities;
using QXAccounting.WebApi.Helpers;

namespace QXAccounting.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    [Authorize(Roles = "Administrator")]
    public class UsersController : Controller
    {
        private IUserRepository _userRepo;
        private IUserFeatureRepository _userFeatureRepo;
        private IUserRoleRepository _userRoleRepo;
        private ILogger<UsersController> _logger;
        private IMapper _mapper;

        public UsersController(
            IUserRepository repo,
            IUserFeatureRepository userFeatureRepo,
            IUserRoleRepository userRoleRepo,
            ILogger<UsersController> logger,
            IMapper mapper
        )
        {
            _userRepo = repo;
            _userFeatureRepo = userFeatureRepo;
            _userRoleRepo = userRoleRepo;
            _logger = logger;
            _mapper = mapper;
        }

        //[HttpGet("{full?}")]
        //public async Task<IActionResult> Get(bool? full)
        //{
        //    var full_ = full ?? false;

        //    var users = full_ ? await _userRepo.GetUsersFullAsync() : await _userRepo.GetAllAsync();

        //    if (!full_)
        //    {
        //        return Ok(_mapper.Map<IEnumerable<User>, IEnumerable<UserVM>>(users));
        //    }

        //    return Ok(users);
        //}

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var users = await _userRepo.GetUsersFullAsync();

            return Ok(users);
        }

        //[HttpGet("GetById/{id}/{full?}")]
        //public async Task<IActionResult> GetById(int id, bool? full)
        //{
        //    if (id <= 0)
        //    {
        //        return BadRequest();
        //    }

        //    var full_ = full ?? false;

        //    var user = full_ ? await _userRepo.GetUserFullAsync(id) : await _userRepo.FindAsync(id);
        //    if (user == null)
        //    {
        //        return NotFound();
        //    }

        //    if (!full_)
        //    {
        //        return Ok(_mapper.Map<User, UserVM>(user));
        //    }

        //    return Ok(user);
        //}

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var user = await _userRepo.GetUserFullAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        //[HttpGet("GetByLoginId/{loginId}/{full?}")]
        //public async Task<IActionResult> GetByLoginId(string loginId, bool? full)
        //{
        //    var full_ = full ?? false;

        //    var user = full_ ? await _userRepo.GetUserFullAsync(loginId) : await _userRepo.GetUserAsync(loginId);
        //    if (user == null)
        //    {
        //        return NotFound();
        //    }

        //    if (!full_)
        //    {
        //        return Ok(_mapper.Map<User, UserVM>(user));
        //    }

        //    return Ok(user);
        //}

        [HttpGet("GetByLoginId/{loginId}")]
        public async Task<IActionResult> Get(string loginId)
        {
            var user = await _userRepo.GetUserFullAsync(loginId);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            //var user = await _userRepo.FindAsync(id);
            var user = await _userRepo.GetUserFullAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            await _userRepo.DeleteAsync(user, Utils.GetClaim(User.Claims, "login_id"));
            return NoContent();
        }

        [HttpDelete("DeleteByLoginId/{loginId}")]
        public async Task<IActionResult> Delete(string loginId)
        {
            //var user = await _userRepo.GetOneAsync(loginId);
            var user = await _userRepo.GetUserFullAsync(loginId);
            if (user == null)
            {
                return NotFound();
            }

            await _userRepo.DeleteAsync(user, Utils.GetClaim(User.Claims, "login_id"));
            return NoContent();
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] NewUserVM newUser)
        {
            if (newUser == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            var newUser_ = _mapper.Map<NewUserVM, User>(newUser);

            var userId = await _userRepo.AddUserAsync(newUser_, newUser.Password, Utils.GetClaim(User.Claims, "login_id"));

            if (userId == 0)
            {
                return BadRequest();
            }

            return CreatedAtRoute("Get", new { id = userId }, newUser_);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] User user)
        {
            if (user == null || user.Id != id /*|| !ModelState.IsValid */)
            {
                return BadRequest();
            }

            await _userRepo.UpdateAsync(user, Utils.GetClaim(User.Claims, "login_id"));

            return CreatedAtAction("Get", new { id = user.Id }, user);

            //var usr = await _userRepo.FindAsync(id);

            //usr.TelephoneNumbers = user.TelephoneNumbers;
            //usr.Email = user.Email;
            //usr.UserName = user.UserName;

            //await _userRepo.UpdateAsync(usr);

            //return CreatedAtAction("GetById", new { id = usr.Id }, usr);
        }

        [HttpPut("UpdateUserRoles/{userId}")]
        public async Task<IActionResult> UpdateUserRoles(int userId, [FromBody] IEnumerable<int> roleIds)
        {
            if (userId <= 0 || roleIds == null)
            {
                return BadRequest();
            }

            var userRoles = await _userRoleRepo.UpdateUserRolesAsync(userId, roleIds, Utils.GetClaim(User.Claims, "login_id"));

            return Ok(userRoles);
        }

        [HttpPut("UpdateUserFeatures/{userId}")]
        public async Task<IActionResult> UpdateUserFeatures(int userId, [FromBody] IEnumerable<int> featureIds)
        {
            if (userId <= 0 || featureIds == null)
            {
                return BadRequest();
            }

            var userFeatures = await _userFeatureRepo.UpdateUserFeaturesAsync(userId, featureIds, Utils.GetClaim(User.Claims, "login_id"));

            return Ok(userFeatures);
        }
    }
}