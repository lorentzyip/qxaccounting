﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.Models.Entities;
using QXAccounting.WebApi.Helpers;
using QXAccounting.WebApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QXAccounting.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Features")]
    [Authorize]
    public class FeaturesController : Controller
    {
        private IFeatureRepository _featureRepo;
        private IRoleFeatureRepository _roleFeatureRepo;
        private IUserFeatureRepository _userFeatureRepo;
        private ILogger<FeaturesController> _logger;
        private IMapper _mapper;

        public FeaturesController(
            IFeatureRepository featureRepo,
            IRoleFeatureRepository roleFeatureRepo,
            IUserFeatureRepository userFeatureRepo,
            ILogger<FeaturesController> logger,
            IMapper mapper
        )
        {
            _featureRepo = featureRepo;
            _roleFeatureRepo = roleFeatureRepo;
            _userFeatureRepo = userFeatureRepo;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var features = await _featureRepo.GetAllAsync();

            return Ok(features);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var feature = await _featureRepo.FindAsync(id);

            if (feature == null)
            {
                return NotFound();
            }

            return Ok(feature);
        }

        [HttpGet("GetByFeatureCode/{featureCode}")]
        public async Task<IActionResult> Get(string featureCode)
        {
            if (featureCode == null)
            {
                return BadRequest();
            }

            var feature = await _featureRepo.GetFeatureAsync(featureCode);

            if (feature == null)
            {
                return NotFound();
            }

            return Ok(feature);
        }

        [Authorize(Roles = "Administrator,Accountant")]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] FeatureVM newFeature)
        {
            if (newFeature == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            var feature = _mapper.Map<FeatureVM, Feature>(newFeature);

            await _featureRepo.AddAsync(feature, Utils.GetClaim(User.Claims, "login_id"));

            return CreatedAtAction("Get", new { id = feature.Id }, feature);
        }

        [Authorize(Roles = "Administrator,Accountant")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Feature feature)
        {
            if (feature == null || id != feature.Id)
            {
                return BadRequest();
            }

            await _featureRepo.UpdateAsync(feature, Utils.GetClaim(User.Claims, "login_id"));

            return CreatedAtAction("Get", new { id = feature.Id }, feature);
        }

        [Authorize(Roles = "Administrator,Accountant")]
        [HttpPut("UpdateRoleFeatures/{roleId}")]
        public async Task<IActionResult> UpdateRoleFeatures(int roleId, [FromBody] IEnumerable<int> featureIds)
        {
            if (roleId <= 0 || featureIds == null)
            {
                return BadRequest();
            }

            return Ok(await _roleFeatureRepo.UpdateRoleFeaturesAsync(roleId, featureIds, Utils.GetClaim(User.Claims, "login_id")));
        }

        [Authorize(Roles = "Administrator,Accountant")]
        [HttpPut("UpdateUserFeatures/{userId}")]
        public async Task<IActionResult> UpdateUserFeatures(int userId, [FromBody] IEnumerable<int> featureIds)
        {
            if (userId <= 0 || featureIds == null)
            {
                return BadRequest();
            }

            return Ok(await _userFeatureRepo.UpdateUserFeaturesAsync(userId, featureIds, Utils.GetClaim(User.Claims, "login_id")));
        }

        [Authorize(Roles = "Administrator,Accountant")]
        [HttpPut("UpdateByFeatureCode/{featureCode}")]
        public async Task<IActionResult> Update(string featureCode, [FromBody] Feature feature)
        {
            if (featureCode == null || feature == null || featureCode != feature.FeatureCode)
            {
                return BadRequest();
            }

            await _featureRepo.UpdateAsync(feature, Utils.GetClaim(User.Claims, "login_id"));

            return CreatedAtAction("Get", new { id = feature.Id }, feature);
        }

        [Authorize(Roles = "Administrator,Accountant")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var feature = await _featureRepo.GetFeatureFullAsync(id);

            if (feature == null)
            {
                return NotFound();
            }

            await _featureRepo.DeleteAsync(feature, Utils.GetClaim(User.Claims, "login_id"));

            return NoContent();
        }

        [Authorize(Roles = "Administrator,Accountant")]
        [HttpDelete("DeleteByFeatureCode/{featureCode}")]
        public async Task<IActionResult> Delete(string featureCode)
        {
            if (featureCode == null)
            {
                return BadRequest();
            }

            var feature = await _featureRepo.GetFeatureFullAsync(featureCode);

            if (feature == null)
            {
                return NotFound();
            }

            await _featureRepo.DeleteAsync(feature, Utils.GetClaim(User.Claims, "login_id"));

            return NoContent();
        }
    }
}
