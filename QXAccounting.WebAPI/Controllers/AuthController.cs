using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using QXAccounting.DAL.Repositories.interfaces;
using QXAccounting.WebApi.Options;
using QXAccounting.WebApi.ViewModels;
using QXAccounting.WebApi.Helpers;

namespace QXAccounting.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Auth")]
    public class AuthController : Controller
    {
        private IUserRepository UserRepo { get; set; }
        private ILogger<AuthController> _logger;
        private JwtIssuerOptions _jwtOptions;

        public AuthController(IUserRepository repo, ILogger<AuthController> logger, IOptions<JwtIssuerOptions> jwtOptions)
        {
            UserRepo = repo;
            _logger = logger;
            _jwtOptions = jwtOptions.Value;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Get()
        {
            return Ok("QX Accounting System");
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginVM login)
        {
            if (!login.IsValid)
            {
                return BadRequest();
            }

            int result = 1;
            int? userId = null;

            using (var connection = new SqlConnection(UserRepo.Context.Database.GetDbConnection().ConnectionString))
            {
                using (var command = new SqlCommand("System.uspUserLogin", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@LoginId", login.LoginId);
                    command.Parameters.AddWithValue("@Password", login.Password);
                    var id = command.Parameters.Add("@Id", SqlDbType.Int);
                    id.Direction = ParameterDirection.Output;
                    var response = command.Parameters.Add("@Response", SqlDbType.NVarChar, 250);
                    response.Direction = ParameterDirection.Output;
                    var retCode = command.Parameters.Add("@ReturnCode", SqlDbType.Int);
                    retCode.Direction = ParameterDirection.ReturnValue;

                    await connection.OpenAsync();
                    await command.ExecuteNonQueryAsync();

                    _logger.LogInformation(response.Value.ToString());
                    result = (int)retCode.Value;
                    
                    if (id.Value != DBNull.Value)
                    {
                        userId = (int)id.Value;
                    }
                }
            }

            if (result != 0 || userId == null)
            {
                return BadRequest();
            }

            var token = await GetJwtSecurityToken(login);
            var refresh_token = Guid.NewGuid().ToString().Replace("-", "");

            var user = await UserRepo.FindAsync(userId);
            user.LastLoginAddress = HttpContext.Connection.RemoteIpAddress.GetAddressBytes();
            user.LastLoginTime = DateTime.Now;
            await UserRepo.UpdateLoginAsync(user);

            return Ok(new
            {
                access_token = new JwtSecurityTokenHandler().WriteToken(token),
                id_token = user.LoginId,
                exp = token.ValidTo,
                refresh_token = refresh_token
            });
        }

        private async Task<JwtSecurityToken> GetJwtSecurityToken(LoginVM login)
        {
            var now = DateTime.UtcNow;

            var user = await UserRepo.GetUserFullAsync(login.LoginId);

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, login.LoginId),
                new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()),
                new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(now).ToString(), ClaimValueTypes.Integer64),
                new Claim("login_id", login.LoginId)
            };

            //var features = new HashSet<int>();

            //foreach (var userRole in user.UserRoles)
            //{
            //    claims.Add(new Claim("Roles", userRole.RoleId.ToString()));
            //    foreach (var roleFeature in userRole.Role.RoleFeatures)
            //    {
            //        features.Add(roleFeature.FeatureId);
            //    }
            //}

            //foreach (var userFeature in user.UserFeatures)
            //{
            //    features.Add(userFeature.FeatureId);
            //}

            //foreach (var feature in features)
            //{
            //    claims.Add(new Claim("Features", feature.ToString()));
            //}

            var features = new HashSet<string>();
            foreach (var userRole in user.UserRoles)
            {
                claims.Add(new Claim("roles", userRole.Role.RoleName));
                foreach (var roleFeature in userRole.Role.RoleFeatures)
                {
                    features.Add(roleFeature.Feature.FeatureCode);
                }
            }
            foreach (var userFeature in user.UserFeatures)
            {
                features.Add(userFeature.Feature.FeatureCode);
            }
            foreach (var feature in features)
            {
                claims.Add(new Claim("features", feature));
            }

            return new JwtSecurityToken(
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.Audience,
                claims: claims,
                notBefore: now,
                expires: now.Add(_jwtOptions.ValidFor),
                signingCredentials: _jwtOptions.SigningCredentials
            );
        }

        private static long ToUnixEpochDate(DateTime date)
            => (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);
    }
}