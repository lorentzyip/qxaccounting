﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QXAccounting.Models.Entities
{
    [Table("Shareholders", Schema = "Accounting")]
    public class Shareholder : EntityBase
    {
        [Required]
        [MaxLength(50)]
        public string ShareholderName { get; set; }

        [Column(TypeName = "decimal(5,2)")]
        public decimal Share { get; set; }

        [MaxLength(300)]
        public string Remarks { get; set; }

        public int CompanyId { get; set; }

        [ForeignKey(nameof(CompanyId))]
        public Company Company { get; set; }
    }
}
