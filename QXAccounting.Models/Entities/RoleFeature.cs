﻿using System.ComponentModel.DataAnnotations.Schema;

namespace QXAccounting.Models.Entities
{
    [Table("RoleFeatures", Schema = "System")]
    public class RoleFeature : EntityBase
    {
        public int RoleId { get; set; }
        public int FeatureId { get; set; }

        [ForeignKey(nameof(RoleId))]
        public Role Role { get; set; }
        [ForeignKey(nameof(FeatureId))]
        public Feature Feature { get; set; }
    }
}
