﻿using System.ComponentModel.DataAnnotations.Schema;

namespace QXAccounting.Models.Entities
{
    [Table("ExchangeHistories", Schema = "Accounting")]
    public class ExchangeHistory : EntityBase
    {
        public int WorkId { get; set; }

        public int CurrencyId { get; set; }

        [Column(TypeName = "decimal(6,3)")]
        public decimal ExchangeRate { get; set; }

        [ForeignKey(nameof(WorkId))]
        public Work Work { get; set; }

        [ForeignKey(nameof(CurrencyId))]
        public Currency Currency { get; set; }
    }
}
