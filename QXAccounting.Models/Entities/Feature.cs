﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QXAccounting.Models.Entities
{
    [Table("Features", Schema = "System")]
    public class Feature : EntityBase
    {
        [Required]
        [MaxLength(50)]
        public string FeatureName { get; set; }
        [Required]
        [MaxLength(10)]
        public string FeatureCode { get; set; }

        [InverseProperty(nameof(RoleFeature.Feature))]
        public ICollection<RoleFeature> RoleFeatures { get; set; } = new HashSet<RoleFeature>();
        [InverseProperty(nameof(UserFeature.Feature))]
        public ICollection<UserFeature> UserFeatures { get; set; } = new HashSet<UserFeature>();
    }
}
