﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QXAccounting.Models.Entities
{
    public abstract class EntityBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string InsertBy { get; set; }
        public DateTime InsertTime { get; set; }

        [MaxLength(50)]
        public string UpdateBy { get; set; }
        public DateTime? UpdateTime { get; set; }

        public bool Deleted { get; set; }
        [MaxLength(50)]
        public string DeleteBy { get; set; }
        public DateTime? DeleteTime { get; set; }

        [Timestamp]
        public byte[] TimeStamp { get; set; }
    }
}
