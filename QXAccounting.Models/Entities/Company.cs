﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QXAccounting.Models.Entities
{
    [Table("Companies", Schema = "Accounting")]
    public class Company : EntityBase
    {
        [Required]
        [MaxLength(50)]
        public string CompanyName { get; set; }

        [Column(TypeName = "decimal(12,4)")]
        public decimal Capital { get; set; }

        [MaxLength(300)]
        public string Remarks { get; set; }

        [InverseProperty(nameof(Shareholder.Company))]
        public ICollection<Shareholder> Shareholders { get; set; } = new HashSet<Shareholder>();

        [InverseProperty(nameof(Work.Company))]
        public ICollection<Work> Works { get; set; } = new HashSet<Work>();
    }
}
