﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QXAccounting.Models.Entities
{
    [Table("Users", Schema = "System")]
    public class User : EntityBase
    {
        [Required]
        [MaxLength(20)]
        public string LoginId { get; set; }

        [Required]
        [MaxLength(50)]
        public string UserName { get; set; }
        [DataType(DataType.PhoneNumber), MaxLength(100)]
        public string TelephoneNumbers { get; set; }
        [DataType(DataType.EmailAddress), MaxLength(50)]
        public string Email { get; set; }

        [Required, Column(TypeName = "binary(64)")]
        public byte[] PasswordHash { get; set; }
        [Required]
        public Guid Salt { get; set; }

        public DateTime? LastLoginTime { get; set; }
        [Column(TypeName = "binary(16)")]
        public byte[] LastLoginAddress { get; set; }

        [InverseProperty(nameof(UserFeature.User))]
        public ICollection<UserFeature> UserFeatures { get; set; } = new HashSet<UserFeature>();
        [InverseProperty(nameof(UserRole.User))]
        public ICollection<UserRole> UserRoles { get; set; } = new HashSet<UserRole>();
    }
}
