﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QXAccounting.Models.Entities
{
    [Table("Roles", Schema = "System")]
    public class Role : EntityBase
    {
        [Required]
        [MaxLength(50)]
        public string RoleName { get; set; }

        [InverseProperty(nameof(RoleFeature.Role))]
        public ICollection<RoleFeature> RoleFeatures { get; set; } = new HashSet<RoleFeature>();
        [InverseProperty(nameof(UserRole.Role))]
        public ICollection<UserRole> UserRoles { get; set; } = new HashSet<UserRole>();
    }
}
