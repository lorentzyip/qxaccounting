﻿using System.ComponentModel.DataAnnotations.Schema;

namespace QXAccounting.Models.Entities
{
    [Table("UserFeatures", Schema = "System")]
    public class UserFeature : EntityBase
    {
        public int UserId { get; set; }
        public int FeatureId { get; set; }

        [ForeignKey(nameof(UserId))]
        public User User { get; set; }
        [ForeignKey(nameof(FeatureId))]
        public Feature Feature { get; set; }
    }
}
