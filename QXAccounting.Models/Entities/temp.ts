﻿export class Company extends EntityBase {
    /*[Required]
[MaxLength(50)]*/
    public CompanyName: string;
    /*[Column(TypeName = "decimal(12,4)")]*/
    public Capital: number;
    /*[MaxLength(300)]*/
    public Remarks: string;
    /*[InverseProperty(nameof(Shareholder.Company))]*/
    public Shareholders: ICollection<Shareholder>;
    /*[InverseProperty(nameof(Work.Company))]*/
    public Works: ICollection<Work>;
}