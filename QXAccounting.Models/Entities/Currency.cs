﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QXAccounting.Models.Entities
{
    [Table("Currencies", Schema = "Accounting")]
    public class Currency : EntityBase
    {
        [Required]
        [MaxLength(20)]
        public string CurrencyName { get; set; }

        [Required]
        [Column(TypeName = "char(3)")]
        public string ISO4217 { get; set; }

        [InverseProperty(nameof(ExchangeHistory.Currency))]
        public ICollection<ExchangeHistory> ExchangeHistories { get; set; } = new HashSet<ExchangeHistory>();
    }
}
