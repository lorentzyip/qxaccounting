﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QXAccounting.Models.Entities
{
    [Table("ClosingHistories", Schema = "Accounting")]
    public class ClosingHistory : EntityBase
    {
        public int WorkId { get; set; }

        [Column(TypeName = "decimal(10, 4)")]
        public decimal ActualPaid { get; set; }

        public short ActualCommissionRate { get; set; }

        [MaxLength(300)]
        public string Remarks { get; set; }

        [ForeignKey(nameof(WorkId))]
        public Work Work { get; set; }
    }
}
